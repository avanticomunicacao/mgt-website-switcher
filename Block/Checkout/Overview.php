<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Checkout;

use Avanti\WebsiteSwitcher\Helper\Data as AvantiData;
use Avanti\WebsiteSwitcher\Helper\System;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\TotalsCollector;
use Magento\Quote\Model\Quote\TotalsReader;
use Magento\Sales\Block\Items\AbstractItems;
use Magento\Store\Model\Information;
use Magento\Tax\Helper\Data;

/**
 * Multishipping checkout overview information
 */
class Overview extends AbstractItems
{
    /**
     * Block alias fallback
     */
    const DEFAULT_TYPE = 'default';

    /**
     * @var \Magento\Multishipping\Model\Checkout\Type\Multishipping
     */
    protected $_multishipping;

    /**
     * @var Data
     */
    protected $_taxHelper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;

    /**
     * @var TotalsCollector
     */
    protected $totalsCollector;

    /**
     * @var TotalsReader
     */
    protected $totalsReader;

    /**
     * @var AvantiData
     */
    private $helperData;

    /**
     * @var Information
     */
    private $_storeInfo;

    /**
     * @var System
     */
    private $system;

    /**
     * @param Context $context
     * @param Multishipping $multishipping
     * @param Data $taxHelper
     * @param PriceCurrencyInterface $priceCurrency
     * @param TotalsCollector $totalsCollector
     * @param TotalsReader $totalsReader
     * @param array $data
     * @param AvantiData $helperData
     * @param Information $_storeInfo
     * @param System $system
     */
    public function __construct(
        Context $context,
        Multishipping $multishipping,
        Data $taxHelper,
        PriceCurrencyInterface $priceCurrency,
        TotalsCollector $totalsCollector,
        TotalsReader $totalsReader,
        array $data = [],
        AvantiData $helperData,
        Information $_storeInfo,
        System $system
    ) {
        $this->_taxHelper = $taxHelper;
        $this->_multishipping = $multishipping;
        $this->priceCurrency = $priceCurrency;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->totalsCollector = $totalsCollector;
        $this->totalsReader = $totalsReader;
        $this->helperData = $helperData;
        $this->_storeInfo = $_storeInfo;
        $this->system = $system;
    }

    /**
     * Initialize default item renderer
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(
            __('Review Order - %1', $this->pageConfig->getTitle()->getDefault())
        );
        return parent::_prepareLayout();
    }

    /**
     * Get multishipping checkout model
     *
     * @return \Magento\Multishipping\Model\Checkout\Type\Multishipping
     */
    public function getCheckout()
    {
        return $this->_multishipping;
    }

    /**
     * @return Address
     */
    public function getBillingAddress()
    {
        return $this->getCheckout()->getQuote()->getBillingAddress();
    }

    /**
     * @return string
     */
    public function getPaymentHtml()
    {
        return $this->getChildHtml('payment_info');
    }

    /**
     * Get object with payment info posted data
     *
     * @return \Magento\Framework\DataObject
     */
    public function getPayment()
    {
        return $this->getCheckout()->getQuote()->getPayment();
    }

    /**
     * @return array
     */
    public function getShippingAddresses()
    {
        $addresses = $this->getCheckout()->getQuote()->getAllShippingAddresses();
        foreach ($addresses as $address)
        {
            foreach ($address->getAllItems() as $item) {
                $address->setData('store_id',$item->getStoreId());
                break;
            }
        }
        return $addresses;
    }

    /**
     * @return int|mixed
     */
    public function getShippingAddressCount()
    {
        $count = $this->getData('shipping_address_count');
        if ($count === null) {
            $count = count($this->getShippingAddresses());
            $this->setData('shipping_address_count', $count);
        }
        return $count;
    }

    /**
     * @param Address $address
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getShippingAddressRate($address)
    {
        $rate = $address->getShippingRateByCode($address->getShippingMethod());
        if ($rate) {
            return $rate;
        }
        return false;
    }

    /**
     * @param Address $address
     * @return mixed
     */
    public function getShippingPriceInclTax($address)
    {
        $exclTax = $address->getShippingAmount();
        $taxAmount = $address->getShippingTaxAmount();
        return $this->formatPrice($exclTax + $taxAmount);
    }

    /**
     * @param Address $address
     * @return mixed
     */
    public function getShippingPriceExclTax($address)
    {
        return $this->formatPrice($address->getShippingAmount());
    }

    /**
     * @param float $price
     * @return mixed
     *
     * @codeCoverageIgnore
     */
    public function formatPrice($price)
    {
        return $this->priceCurrency->format(
            $price,
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $this->getQuote()->getStore()
        );
    }

    /**
     * @param Address $address
     * @return array
     */
    public function getShippingAddressItems($address): array
    {
        return $address->getAllVisibleItems();
    }

    /**
     * @param Address $address
     * @return mixed
     */
    public function getShippingAddressTotals($address)
    {
        $totals = $address->getTotals();
        foreach ($totals as $total) {
            if ($total->getCode() == 'grand_total') {
                if ($address->getAddressType() == Address::TYPE_BILLING) {
                    $total->setTitle(__('Total'));
                } else {
                    $total->setTitle(__('Total for this address'));
                }
            }
        }
        return $totals;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->getCheckout()->getQuote()->getGrandTotal();
    }

    /**
     * @return string
     */
    public function getAddressesEditUrl()
    {
        return $this->getUrl('*/*/backtoaddresses');
    }

    /**
     * @param Address $address
     * @return string
     */
    public function getEditShippingAddressUrl($address)
    {
        return $this->getUrl('*/checkout_address/editShipping', ['id' => $address->getCustomerAddressId()]);
    }

    /**
     * @param Address $address
     * @return string
     */
    public function getEditBillingAddressUrl($address)
    {
        return $this->getUrl('*/checkout_address/editBilling', ['id' => $address->getCustomerAddressId()]);
    }

    /**
     * @return string
     */
    public function getEditShippingUrl()
    {
        return $this->getUrl('*/*/backtoshipping');
    }

    /**
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/overviewPost');
    }

    /**
     * @return string
     */
    public function getEditBillingUrl()
    {
        return $this->getUrl('*/*/backtobilling');
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/backtobilling');
    }

    /**
     * Retrieve virtual product edit url
     *
     * @return string
     */
    public function getVirtualProductEditUrl()
    {
        return $this->getUrl('checkout/cart');
    }

    /**
     * Retrieve virtual product collection array
     *
     * @return array
     */
    public function getVirtualItems()
    {
        return $this->getBillingAddress()->getAllVisibleItems();
    }

    /**
     * Retrieve quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->getCheckout()->getQuote();
    }

    /**
     * @deprecated 100.2.3
     * typo in method name, see getBillingAddressTotals()
     * @return mixed
     */
    public function getBillinAddressTotals()
    {
        return $this->getBillingAddressTotals();
    }

    /**
     * @return mixed
     * @since 100.2.3
     */
    public function getBillingAddressTotals()
    {
        $address = $this->getQuote()->getBillingAddress();
        return $this->getShippingAddressTotals($address);
    }

    /**
     * @param mixed $totals
     * @param null $colspan
     * @return string
     */
    public function renderTotals($totals, $colspan = null)
    {
        if ($colspan === null) {
            $colspan = 3;
        }
        $totals = $this->getChildBlock(
            'totals'
        )->setTotals(
            $totals
        )->renderTotals(
            '',
            $colspan
        ) . $this->getChildBlock(
            'totals'
        )->setTotals(
            $totals
        )->renderTotals(
            'footer',
            $colspan
        );
        return $totals;
    }

    /**
     * Return row-level item html
     *
     * @param \Magento\Framework\DataObject $item
     * @return string
     */
    public function getRowItemHtml(\Magento\Framework\DataObject $item)
    {
        $type = $this->_getItemType($item);
        $renderer = $this->_getRowItemRenderer($type)->setItem($item);
        $this->_prepareItem($renderer);
        return $renderer->toHtml();
    }

    /**
     * Retrieve renderer block for row-level item output
     *
     * @param string $type
     * @return \Magento\Framework\View\Element\AbstractBlock
     */
    protected function _getRowItemRenderer($type)
    {
        $renderer = $this->getItemRenderer($type);
        if ($renderer !== $this->getItemRenderer(self::DEFAULT_TYPE)) {
            $renderer->setTemplate($this->getRowRendererTemplate());
        }
        return $renderer;
    }

    /**
     * @param null $id
     * @return string
     */
    public function getStoreName($id = null)
    {
        if (is_null($id)) {
            $id = $this->helperData->getStoreId();
        }
        return $this->helperData->getStoreById($id)->getName();
    }

    /**
     * @param $address
     * @return string
     */
    public function getFormattedStoreAddress($address)
    {
        return $this->_storeInfo->getFormattedAddress($this->helperData->getStoreById($address->getData('store_id')));
    }

    /**
     * @return false|string[]
     */
    public function getShippingInformation()
    {
        return $this->system->shippingInformation();
    }
}
