<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Checkout\ConfigurableProduct\Cart\Item\Renderer;

use Magento\Catalog\Block\Product\ImageBuilder;
use Magento\Catalog\Helper\Product\Configuration;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Magento\Checkout\Model\Session;
use Magento\ConfigurableProduct\Block\Cart\Item\Renderer\Configurable as MageConfigurable;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Module\Manager;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Url\Helper\Data as MageUrlData;
use Magento\Framework\View\Element\Message\InterpretationStrategyInterface;
use Avanti\WebsiteSwitcher\Helper\Data;
use Magento\Framework\View\Element\Template\Context;

/**
 * Class Configurable
 */
class Configurable extends MageConfigurable
{
    /**
     * @var Data
     */
    protected $helperData;

    /**
     * Configurable constructor.
     * @param Context $context
     * @param Configuration $productConfig
     * @param Session $checkoutSession
     * @param ImageBuilder $imageBuilder
     * @param MageUrlData $urlHelper
     * @param ManagerInterface $messageManager
     * @param PriceCurrencyInterface $priceCurrency
     * @param Manager $moduleManager
     * @param InterpretationStrategyInterface $messageInterpretationStrategy
     * @param ItemResolverInterface|null $itemResolver
     * @param Data $helperData
     * @param array $data
     */
    public function __construct(
        Context $context,
        Configuration $productConfig,
        Session $checkoutSession,
        ImageBuilder $imageBuilder,
        MageUrlData $urlHelper,
        ManagerInterface $messageManager,
        PriceCurrencyInterface $priceCurrency,
        Manager $moduleManager,
        InterpretationStrategyInterface $messageInterpretationStrategy,
        ItemResolverInterface $itemResolver = null,
        Data $helperData,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $productConfig,
            $checkoutSession,
            $imageBuilder,
            $urlHelper,
            $messageManager,
            $priceCurrency,
            $moduleManager,
            $messageInterpretationStrategy,
            $data,
            $itemResolver
        );
        $this->helperData = $helperData;
    }

    /**
     * @return mixed
     */
    public function getStoreName()
    {
        $itemQuote = $this->getItem()->getQuote()->getItemById($this->getItem()->getId());
        $storeId = $itemQuote->getStoreId();
        foreach ($this->helperData->getStores() as $store) {
            if ($storeId === (int)$store['value']) {
                return $store['label'];
            }
        }
    }
}