<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Checkout;

use Avanti\WebsiteSwitcher\Api\InventoryInterface;
use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\Data as AvantiData;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Filter\DataObject\GridFactory;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Multishipping\Model\Checkout\Type\Multishipping;
use Magento\Quote\Model\Quote\Address;
use Magento\Sales\Block\Items\AbstractItems;
use Magento\Store\Model\Information;
use Magento\Tax\Helper\Data;

/**
 * Mustishipping checkout shipping
 */
class Shipping extends AbstractItems
{
    /**
     * @var GridFactory
     */
    protected $_filterGridFactory;

    /**
     * @var Data
     */
    protected $_taxHelper;

    /**
     * @var PriceCurrencyInterface
     */
    protected $priceCurrency;
    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;
    /**
     * @var AvantiData
     */
    private $helperData;
    /**
     * @var InventoryInterface
     */
    private $inventory;

    /**
     * @var Information
     */
    private $_storeInfo;
    /**
     * @var System
     */
    private $system;

    /**
     * @param Context $context
     * @param GridFactory $filterGridFactory
     * @param Multishipping $multishipping
     * @param Data $taxHelper
     * @param PriceCurrencyInterface $priceCurrency
     * @param array $data
     * @param QuoteHandlerInterface $quoteHandler
     * @param AvantiData $helperData
     * @param InventoryInterface $inventory
     * @param Information $_storeInfo
     * @param System $system
     */
    public function __construct(
        Context $context,
        GridFactory $filterGridFactory,
        Multishipping $multishipping,
        Data $taxHelper,
        PriceCurrencyInterface $priceCurrency,
        array $data = [],
        QuoteHandlerInterface $quoteHandler,
        AvantiData $helperData,
        InventoryInterface $inventory,
        Information $_storeInfo,
        System $system
    ) {
        $this->priceCurrency = $priceCurrency;
        $this->_taxHelper = $taxHelper;
        $this->_filterGridFactory = $filterGridFactory;
        $this->_multishipping = $multishipping;
        parent::__construct($context, $data);
        $this->_isScopePrivate = true;
        $this->quoteHandler = $quoteHandler;
        $this->helperData = $helperData;
        $this->inventory = $inventory;
        $this->_storeInfo = $_storeInfo;
        $this->system = $system;
    }

    /**
     * Get multishipping checkout model
     *
     * @return Multishipping
     */
    public function getCheckout()
    {
        return $this->_multishipping;
    }

    /**
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(
            __('Shipping Methods') . ' - ' . $this->pageConfig->getTitle()->getDefault()
        );
        return parent::_prepareLayout();
    }

    /**
     * @return Address[]
     */
    public function getAddresses()
    {
        return $this->getCheckout()->getQuote()->getAllShippingAddresses();
    }

    /**
     * @return mixed
     */
    public function getAddressCount()
    {
        $count = $this->getData('address_count');
        if ($count === null) {
            $count = count($this->getAddresses());
            $this->setData('address_count', $count);
        }
        return $count;
    }

    /**
     * @param Address $address
     * @return \Magento\Framework\DataObject[]
     */
    public function getAddressItems($address)
    {
        $items = [];
        foreach ($address->getAllItems() as $item) {
            if ($item->getParentItemId()) {
                continue;
            }
            $item->setQuoteItem($this->getCheckout()->getQuote()->getItemById($item->getQuoteItemId()));
            $items[] = $item;
        }
        $itemsFilter = $this->_filterGridFactory->create();
        $itemsFilter->addFilter(new \Magento\Framework\Filter\Sprintf('%d'), 'qty');
        return $itemsFilter->filter($items);
    }

    /**
     * @param Address $address
     * @return mixed
     */
    public function getAddressShippingMethod($address)
    {
        return $address->getShippingMethod();
    }

    /**
     * @param Address $address
     * @return mixed
     */
    public function getShippingRates($address)
    {
        $groups = $address->getGroupedAllShippingRates();
        return $groups;
    }

    /**
     * @param string $carrierCode
     * @return string
     */
    public function getCarrierName($carrierCode)
    {
        if ($name = $this->_scopeConfig->getValue(
            'carriers/' . $carrierCode . '/title',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        )
        ) {
            return $name;
        }
        return $carrierCode;
    }

    /**
     * @param Address $address
     * @return string
     */
    public function getAddressEditUrl($address)
    {
        return $this->getUrl('*/checkout_address/editShipping', ['id' => $address->getCustomerAddressId()]);
    }

    /**
     * @return string
     */
    public function getItemsEditUrl()
    {
        return $this->getUrl('*/*/backToAddresses');
    }

    /**
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/shippingPost');
    }

    /**
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('checkout/cart');
    }

    /**
     * @param Address $address
     * @param float $price
     * @param bool $flag
     * @return float
     */
    public function getShippingPrice($address, $price, $flag)
    {
        return $this->priceCurrency->convertAndFormat(
            $this->_taxHelper->getShippingPrice($price, $flag, $address),
            true,
            PriceCurrencyInterface::DEFAULT_PRECISION,
            $address->getQuote()->getStore()
        );
    }

    /**
     * Retrieve text for items box
     *
     * @param \Magento\Framework\DataObject $addressEntity
     * @return string
     *
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function getItemsBoxTextAfter(\Magento\Framework\DataObject $addressEntity)
    {
        return '';
    }

    /**
     * @return array|bool
     */
    public function getSplitedQuote()
    {

        $quotes = $this->quoteHandler->splitQuotes($this->getCheckout()->getQuote());
        $this->setData('quote_count',count($this->quoteHandler->splitQuotes($this->getCheckout()->getQuote())));
        return $quotes;
    }

    /**
     * @return array|int|mixed
     */
    public function getStoresCount()
    {
        $count = $this->getData('quote_count');
        if ($count === null) {
            $this->getSplitedQuote();
            $count = $this->getData('quote_count');
        }
        return $count;
    }

    /**
     * @param null $id
     * @return string
     */
    public function getStoreName($id = null)
    {
        if (is_null($id)) {
            $id = $this->helperData->getStoreId();
        }
        return $this->helperData->getStoreById($id)->getName();
    }

    /**
     * @param null $storedId
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSourceAddress($storedId = null)
    {
        if (is_null($storedId)) {
            $storedId = $this->helperData->getStoreId();
        }
        $website = $this->helperData->getWebsiteByStoreId($storedId);
        $stock = $this->inventory->getStockByWebsite($website);
        $source = $this->inventory->getAssignedSource($stock->getStockId());
        return $this->inventory->getSourceAddress($source->getSourceCode());
    }

    /**
     * @param $address
     * @return string
     */
    public function getFormattedStoreAddress($address)
    {
        return $this->_storeInfo->getFormattedAddress($this->helperData->getStoreById($address->getData('store_id')));
    }

    /**
     * @return false|string[]
     */
    public function getShippingInformation()
    {
        return $this->system->shippingInformation();
    }
}
