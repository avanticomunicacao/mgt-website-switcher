<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Checkout;

use Magento\Checkout\Model\Session;
use Magento\Framework\View\Element\Template\Context;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;
use Magento\Payment\Block\Form\Container;
use Magento\Payment\Helper\Data;
use Magento\Payment\Model\Checks\SpecificationFactory;
use Magento\Payment\Model\Method\SpecificationInterface;

/**
 * Multishipping billing information
 */
class Billing extends Container
{
    /**
     * @var Multishipping
     */
    protected $_multishipping;

    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * @var SpecificationInterface
     */
    protected $paymentSpecification;

    /**
     * @param Context $context
     * @param Data $paymentHelper
     * @param SpecificationFactory $methodSpecificationFactory
     * @param Multishipping $multishipping
     * @param Session $checkoutSession
     * @param SpecificationInterface $paymentSpecification
     * @param array $data
     * @param array $additionalChecks
     */
    public function __construct(
        Context $context,
        Data $paymentHelper,
        SpecificationFactory $methodSpecificationFactory,
        Multishipping $multishipping,
        Session $checkoutSession,
        SpecificationInterface $paymentSpecification,
        array $data = [],
        array $additionalChecks = []
    ) {
        $this->_multishipping = $multishipping;
        $this->_checkoutSession = $checkoutSession;
        $this->paymentSpecification = $paymentSpecification;
        parent::__construct($context, $paymentHelper, $methodSpecificationFactory, $data, $additionalChecks);
        $this->_isScopePrivate = true;
    }

    /**
     * Prepare children blocks
     *
     * @return $this
     */
    protected function _prepareLayout()
    {
        $this->pageConfig->getTitle()->set(
            __('Billing Information - %1', $this->pageConfig->getTitle()->getDefault())
        );

        return parent::_prepareLayout();
    }

    /**
     * Check payment method model
     *
     * @param \Magento\Payment\Model\Method\AbstractMethod|null $method
     * @return bool
     */
    protected function _canUseMethod($method)
    {
        return $method && $this->paymentSpecification->isSatisfiedBy(
                $method->getCode()
            ) && parent::_canUseMethod(
                $method
            );
    }

    /**
     * Retrieve code of current payment method
     *
     * @return mixed
     */
    public function getSelectedMethodCode()
    {
        $method = $this->getQuote()->getPayment()->getMethod();
        if ($method) {
            return $method;
        }
        return false;
    }

    /**
     * Retrieve billing address
     *
     * @return \Magento\Quote\Model\Quote\Address
     */
    public function getAddress()
    {
        $address = $this->getData('address');
        if ($address === null) {
            $address = $this->_multishipping->getQuote()->getBillingAddress();
            $this->setData('address', $address);
        }
        return $address;
    }

    /**
     * Retrieve quote model object
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->_checkoutSession->getQuote();
    }

    /**
     * Getter
     *
     * @return float
     */
    public function getQuoteBaseGrandTotal()
    {
        return (double)$this->getQuote()->getBaseGrandTotal();
    }

    /**
     * Retrieve url for select billing address
     *
     * @return string
     */
    public function getSelectAddressUrl()
    {
        return $this->getUrl('*/checkout_address/selectBilling');
    }

    /**
     * Retrieve data post destination url
     *
     * @return string
     */
    public function getPostActionUrl()
    {
        return $this->getUrl('*/*/overview');
    }

    /**
     * Retrieve back url
     *
     * @return string
     */
    public function getBackUrl()
    {
        return $this->getUrl('*/*/backtoshipping');
    }
}