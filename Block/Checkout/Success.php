<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Checkout;

use Magento\Checkout\Block\Onepage\Success as MageOnepageSuccess;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Element\Template\Context;
use Magento\Checkout\Model\Session;
use Magento\Sales\Model\Order\Config;
use Magento\Framework\App\Http\Context as HttpContext;
use Magento\Framework\Registry;
use Magento\Sales\Model\OrderRepository;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Sales\Model\Order;

/**
 * Class Success
 * Overriding Magento One page success
 */
class Success extends MageOnepageSuccess implements ArgumentInterface
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var Registry
     */
    private $_coreRegistry;

    /**
     * @var OrderRepository
     */
    private $_orderRepository;

    /**
     * @param Context $context
     * @param Session $checkoutSession
     * @param Config $orderConfig
     * @param HttpContext $httpContext
     * @param Registry $registry
     * @param OrderRepository $orderRepository
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param array $data
     */
    public function __construct(
        Context $context,
        Session $checkoutSession,
        Config $orderConfig,
        HttpContext $httpContext,
        Registry $registry,
        OrderRepository $orderRepository,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $checkoutSession,
            $orderConfig,
            $httpContext,
            $data
        );
        $this->checkoutSession = $checkoutSession;
        $this->_coreRegistry = $registry;
        $this->_orderRepository = $orderRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
    }

    /**
     * @return bool|array
     */
    public function getOrderArray()
    {
        $splitOrders = $this->checkoutSession->getOrderIds();

        if (empty($splitOrders) || count($splitOrders) <= 1) {
            return false;
        }
        return $splitOrders;
    }

    /**
     *
     */
    public function unsOrderIds()
    {
        $this->checkoutSession->unsOrderIds();
    }

    /**
     * @return \Magento\Sales\Model\Order
     */
    public function getOrderByIcrementId($incrementId) : Order
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter('increment_id', $incrementId, 'eq')
            ->setPageSize(1)
            ->setCurrentPage(1)
            ->create();
        $orderList = $this->_orderRepository->getList($searchCriteria)->getItems();
        foreach ($orderList as $item) {
            $order = $item;
        }
        return $item;
    }

    /**
     * @param Order $order
     */
    public function setCurrentOrder(Order $order)
    {
        $register = $this->_coreRegistry->registry('current_order');
        if (isset($register)) {
            $this->_coreRegistry->unregister('current_order');
        }
        $this->_coreRegistry->register('current_order', $order);
    }
}