<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Switcher;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;

/**
 * Provide information to frontend website switcher modal
 */
class Modal extends Template
{
    /**
     * @var System
     */
    private $system;

    public function __construct(
        Context $context,
        System $system,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->system = $system;
    }

    /**
     * @return bool
     */
    public function showModal() : bool
    {
        return $this->system->showModal();
    }

    /**
     * @return bool
     */
    public function titleModal() : string
    {
        return $this->system->titleModal();
    }

    /**
     * @return string
     */
    public function getContent() : string
    {
        return $this->system->warningModalText();
    }

    /**
     * @return string
     */
    public function getButtonAccepted() : string
    {
        return $this->system->acceptedModalTextButton();
    }

    /**
     * @return string
     */
    public function getButtonDecline() : string
    {
        return $this->system->declineModalTextButton();
    }
}