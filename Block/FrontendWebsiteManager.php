<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\App\Config;
use Magento\Framework\View\Element\Template\Context;
use Avanti\WebsiteSwitcher\Helper\Data;
use Magento\Framework\View\Element\Template;
use Avanti\WebsiteSwitcher\Helper\Config as HelperConfig;

/**
 * Provide information to frontend storage manager
 */
class FrontendWebsiteManager extends Template
{
    /**
     * @var FrontendStorageConfigurationPool
     */
    private $storageConfigurationPool;

    /**
     * @var Config
     */
    private $appConfig;

    /**
     * @var Data
     */
    private $data;
    /**
     * @var System
     */
    private $system;

    /**
     * @param Context $context
     * @param Config $appConfig
     * @param Data $helperData
     * @param System $system
     * @param array $data
     */
    public function __construct(
        Context $context,
        Config $appConfig,
        Data $helperData,
        System $system,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->appConfig = $appConfig;
        $this->data = $helperData;
        $this->system = $system;
    }

    /**
     * @return mixed
     */
    public function getWebsites()
    {
        return $this->data->getWebsites();
    }

    /**
     * @return mixed
     */
    public function isEnabledChangeWebsite()
    {
        return $this->system->isEnabledChangeWebsite();
    }

    /**
     * @return string
     */
    public function getFormActionUrl()
    {
        return $this->getUrl(HelperConfig::WEBSITESWITCHER_WEBSITE_CHANGEACTION, ['_secure' => true]);
    }

    /**
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWebsite()
    {
        return $this->data->getWebsite();
    }

    /**
     * @return string
     */
    public function getStore()
    {
        return $this->data->getStoreCode();
    }

    /**
     * @return false|string[]
     */
    public function getWebsiteLocal()
    {
        return $this->system->changeWebsiteLocal();
    }
}