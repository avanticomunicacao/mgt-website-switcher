<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\DataProviders;

use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Checkout\Model\CompositeConfigProvider;
use Magento\Customer\Model\Address\Config as AddressConfig;
use Magento\Framework\Serialize\Serializer\Json as Serializer;
use Magento\Quote\Model\Quote\Address;

/**
 * Provide information to frontend storage manager
 */
class Billing implements ArgumentInterface
{
    /**
     * @var AddressConfig
     */
    private $addressConfig;

    /**
     * @var CompositeConfigProvider
     */
    private $configProvider;

    /**
     * @var Serializer
     */
    private $serializer;

    /**
     * @param AddressConfig $addressConfig
     * @param CompositeConfigProvider $configProvider
     * @param Serializer $serializer
     */
    public function __construct(
        AddressConfig $addressConfig,
        CompositeConfigProvider $configProvider,
        Serializer $serializer
    ) {
        $this->addressConfig = $addressConfig;
        $this->configProvider = $configProvider;
        $this->serializer = $serializer;
    }

    /**
     * Get address formatted as html string.
     *
     * @param Address $address
     * @return string
     */
    public function getAddressHtml(Address $address): string
    {
        $renderer = $this->addressConfig->getFormatByCode('html')->getRenderer();

        return $renderer->renderArray($address->getData());
    }

    /**
     * Returns serialized checkout config.
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public function getSerializedCheckoutConfigs(): string
    {
        return $this->serializer->serialize($this->configProvider->getConfig());
    }
}