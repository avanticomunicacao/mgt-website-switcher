<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Block\Product\Price;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Magento\Catalog\Model\Product;
use Avanti\WebsiteSwitcher\Helper\Data;

class ListView extends Template
{

    /**
     * @var  Magento\Catalog\Model\Product
     */
    public $_product;

    /**
     * @var Data
     */
    protected $helperData;

    /**
     * @var PriceCurrencyInterface
     */
    private $priceCurrency;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;
    /**
     * @var System
     */
    private $system;

    /**
     * ListView constructor.
     * @param Context $context
     * @param Data $helperData
     * @param PriceCurrencyInterface $priceCurrency
     * @param ProductRepositoryInterface $productRepository
     * @param System $system
     */
    public function __construct(
        Context $context,
        Data $helperData,
        PriceCurrencyInterface $priceCurrency,
        ProductRepositoryInterface $productRepository,
        System $system
    ) {
        parent::__construct($context);
        $this->helperData = $helperData;
        $this->priceCurrency = $priceCurrency;
        $this->productRepository = $productRepository;
        $this->system = $system;
    }

    /**
     * @return bool
     */
    public function isEnabled() : bool
    {
        return ($this->system->isEnabled() && $this->system->isCheapestStoreEnabled());
    }

    /**
     * @param Product $product
     */
    public function setProduct(Product $product)
    {
        $this->_product = $product;
    }

    /**
     * @param Product $product
     */
    public function setCurrentProduct(Product $product)
    {
        $this->helperData->setCurrentProduct($product);
    }

    /**
     * @return array|null
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getMinPrice()
    {
        $minPrice['price'] = $this->helperData->getCurrentProduct()->getPriceInfo()->getPrice('final_price')->getValue();
        $minPrice['store_id'] = $this->helperData->getStoreId();
        foreach ($this->getProductsByStores() as $product) {
            switch ($product->getTypeId()) {
                case \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE :
                    foreach ($product->getTypeInstance()->getUsedProducts($product) as $children) {
                        if ($this->checkMinPrice($minPrice,$children)) {
                            $minPrice = $this->setMinPrice($minPrice,$children);
                        }
                    }
                    break;
                default :
                    if ($this->checkMinPrice($minPrice,$product)) {
                        $minPrice = $this->setMinPrice($minPrice,$product);
                    }
            }
        }
        return (((int)$minPrice['store_id'] == (int)$this->helperData->getStoreId())) ? null : $this->detailedInfoPrice($minPrice);
    }

    /**
     * @param $minPrice
     * @param $product
     * @return bool
     */
    public function checkMinPrice($minPrice, $product)
    {
        if ((float)$minPrice['price'] > (float)$product->getPriceInfo()->getPrice('final_price')->getValue() && (bool)$product->isAvailable()) {
            return true;
        }
        return false;
    }

    /**
     * @param $minPrice
     * @param $product
     * @return mixed
     */
    public function setMinPrice($minPrice, $product)
    {
        $minPrice['price'] = $product->getPriceInfo()->getPrice('final_price')->getValue();
        $minPrice['store_id'] = $product->getStoreId();
        $minPrice['store_name'] = $this->getStoreName($product->getStoreId());
        $minPrice['label'] = __('Special Offer');
        return $minPrice;
    }

    /**
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductsByStores()
    {
        $storeCode = $this->helperData->getStoreId();
        $products = [];
        foreach ($this->helperData->getStores() as $store)
        {
            $this->helperData->changeStore($store['value']);
            $products[$store['value']] = $this->productRepository->get(
                $this->_product->getSku(),
                false,
                $store['value'],
                true);
        }
        $this->helperData->changeStore($storeCode);
        return $products;
    }

    /**
     * @param $storeId
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProductByStore($storeId)
    {
        return $this->productRepository->get(
                $this->_product->getSku(),
               false,
                $storeId,
                true);
    }

    /**
     * @param $storeId
     * @return mixed|string
     */
    public function getStoreName($storeId)
    {
        foreach ($this->helperData->getStores() as $store) {
            if ((int)$storeId === (int)$store['value']) {
                return $store['label'];
            }
        }
        return 'default';
    }

    /**
     * @param $minPrice
     * @return array
     */
    public function detailedInfoPrice($minPrice)
    {
        $current = array(
            'price' => $this->helperData->getCurrentProduct()->getPriceInfo()->getPrice('final_price')->getValue(),
            'store_id' => $this->helperData->getStoreId(),
            'store_name' => $this->getStoreName($this->helperData->getStoreId()),
            'label' => __('Actual Price')
        );
        return array(
            $current,$minPrice
        );
    }

    /**
     * Format price value
     *
     * @param float $amount
     * @param bool $includeContainer
     * @param int $precision
     * @return float
     */
    public function formatCurrency(
        $amount,
        $includeContainer = true,
        $precision = PriceCurrencyInterface::DEFAULT_PRECISION
    ) {
        return $this->priceCurrency->format($amount, $includeContainer, $precision);
    }

    /**
     * @param $id
     * @return \Avanti\WebsiteSwitcher\Helper\Magento\Catalog\Model\ProductFactory
     */
    public function getProducById($id)
    {
        return $this->helperData->getProducById($id);
    }

    /**
     * @return int
     */
    public function getStoreId()
    {
        return $this->helperData->getStoreId();
    }

    /**
     * @param $id
     */
    public function setStoreId($id)
    {
        $this->helperData->changeStore($id);
    }

}
