<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Helper;

use Magento\Framework\Url\Helper\Data as MageHelper;

/**
 * Class Url
 */
class Url extends MageHelper
{
    /**
     * Retrieve shopping cart url
     *
     * @return string
     */
    public function getCartUrl()
    {
        return $this->_getUrl('checkout/cart');
    }

    /**
     * Retrieve checkout url
     *
     * @return string
     */
    public function getMSCheckoutUrl()
    {
        return $this->_getUrl('websiteswitcher/checkout');
    }

    /**
     * Retrieve login url
     *
     * @return string
     */
    public function getMSLoginUrl()
    {
        return $this->_getUrl('customer/account/login', ['_secure' => true, '_current' => true]);
    }

    /**
     * Retrieve address url
     *
     * @return string
     */
    public function getMSAddressesUrl()
    {
        return $this->_getUrl('websiteswitcher/checkout/addresses');
    }

    /**
     * Retrieve shipping address save url
     *
     * @return string
     */
    public function getMSShippingAddressSavedUrl()
    {
        return $this->_getUrl('websiteswitcher/checkout_address/shippingSaved');
    }

    /**
     * Retrieve register url
     *
     * @return string
     */
    public function getMSNewShippingUrl()
    {
        return $this->_getUrl('websiteswitcher/checkout_address/newShipping');
    }

    /**
     * Retrieve register url
     *
     * @return string
     */
    public function getMSRegisterUrl()
    {
        return $this->_getUrl('websiteswitcher/checkout/register');
    }
}