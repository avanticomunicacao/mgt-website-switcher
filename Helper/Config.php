<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

namespace Avanti\WebsiteSwitcher\Helper;

/**
 * Class Config
 */
class Config
{
    const XML_PATH_WEBSITE_SWITCHER_ENABLED = 'website_switcher/general/enable';
    const XML_PATH_WEBSITE_SWITCHER_SHARE_QUOTE = 'website_switcher/general/share_quote';
    const XML_PATH_WEBSITE_SWITCHER_CHEAPEST_STORE = 'website_switcher/general/cheapest_website';
    const XML_PATH_WEBSITE_SWITCHER_SPLIT_ORDER = 'website_switcher/general/split_order';
    const XML_PATH_WEBSITE_SWITCHER_SUCCESS_PAGE = 'website_switcher/general/success_page';
    const XML_PATH_WEBSITE_SWITCHER_CHANGE_WEBSITE = 'website_switcher/general/change_website';
    const XML_PATH_WEBSITE_SWITCHER_CHANGE_WEBSITE_LOCAL = 'website_switcher/general/website_local';
    const XML_PATH_WEBSITE_SWITCHER_UPDATE_CART = 'website_switcher/minicart/update_cart';
    const XML_PATH_WEBSITE_SWITCHER_CLEAR_CART = 'website_switcher/minicart/clear_cart';
    const XML_PATH_WEBSITE_SWITCHER_UPDATE_ADD_PRODUCT = 'website_switcher/minicart/update_products_in_add';
    const XML_PATH_WEBSITE_SWITCHER_SHOW_MODAL = 'website_switcher/minicart/show_modal';
    const XML_PATH_WEBSITE_SWITCHER_MODAL_TITLE = 'website_switcher/minicart/title_modal';
    const XML_PATH_WEBSITE_SWITCHER_WARNING_MODAL = 'website_switcher/minicart/warning_modal';
    const XML_PATH_WEBSITE_SWITCHER_MODAL_CONFIRM_TEXT = 'website_switcher/minicart/modal_confirm_text';
    const XML_PATH_WEBSITE_SWITCHER_MODAL_DECLINE_TEXT = 'website_switcher/minicart/modal_decline_text';
    const XML_PATH_WEBSITE_SWITCHER_CHECKOUT_MULTIPLE_AVAILABLE = 'website_switcher/shipping/enable';
    const XML_PATH_WEBSITE_SWITCHER_CHECKOUT_MULTIPLE_INFORMATION = 'website_switcher/shipping/show_information';
    const XML_PATH_WEBSITE_SWITCHER_CHECKOUT_MULTIPLE_MAXIMUM_QUANTITY = 'website_switcher/shipping/checkout_multiple_maximum_qty';
    const XML_PATH_WEBSITE_SWITCHER_COOKIE_NAME = 'website_switcher/cookie/name';
    const XML_PATH_WEBSITE_SWITCHER_COOKIE_DURATION = 'website_switcher/cookie/duration';
    const WEBSITESWITCHER_WEBSITE_CHANGEACTION = 'websiteswitcher/website/changeaction';
    const TEMPLATE_PRODUCT_PRICE_FINAL_PRICE = 'Magento_Catalog::product/price/final_price.phtml';
    const TEMPLATE_PRODUCT_VIEW_FORMTABS = 'Avanti_WebsiteSwitcher::product/view/form-tabs.phtml';
    const TEMPLATE_CONFIGURABLE_PRODUCT_FINAL_PRICE = 'Magento_ConfigurableProduct::product/price/final_price.phtml';
    const TEMPLATE_PRODUCT_OPTIONS = 'Avanti_WebsiteSwitcher::product/view/options.phtml';
    const TEMPLATE_PRODUCT_VIEW_OPTIONS_WRAPPER = 'Avanti_WebsiteSwitcher::product/view/options/wrapper.phtml';
    const SWATCH_RENDERER_TEMPLATE = 'Avanti_WebsiteSwitcher::product/view/renderer.phtml';
    const MAGENTO_THEME_INDENTIFICATION_TEMPLATE = 'Magento_Theme::html/header/location/warning.phtml';
    const MAGENTO_THEME_INDENTIFICATION_INFO_TEMPLATE = 'Magento_Theme::html/header/location/info.phtml';
    const ALLL_QUOTE_ID = 'all_quote_id';
    const STEP_SELECT_ADDRESSES = 'avanti_shipping_addresses';
    const STEP_SHIPPING = 'avanti_shipping_shipping';
    const STEP_BILLING = 'avanti_shipping_billing';
    const STEP_OVERVIEW = 'avanti_shipping_overview';
    const STEP_SUCCESS = 'avanti_shipping_success';
    const STEP_RESULTS = 'avanti_shipping_results';
    const ONE_PAGE = 'one_page';
    const METHOD_CONFIGURABLE_PRODUCT_MODEL_GET_USED_PRODUCTS = 'Magento\ConfigurableProduct\Model\Product\Type\Configurable::getUsedProducts';
    const METHOD_CONFIGURABLE_PRODUCT_MODEL_GET_SALABLE_USED_PRODUCTS = 'Magento\ConfigurableProduct\Model\Product\Type\Configurable::getSalableUsedProducts';
    const FILE_PUB_INDEX = './pub/index.php';
    const FILE_PUB_INDEX2 = './pub/index2.php';
    const MAGE_QUOTE_API_DATA_SHIPPING_METHOD_INTERFACE = '\Magento\Quote\Api\Data\ShippingMethodInterface[]';
}
