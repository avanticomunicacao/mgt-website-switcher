<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Helper;

use Magento\Catalog\Model\Product;
use Magento\Directory\Helper\Data as MagentoData;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Registry;
use Magento\Catalog\Model\ProductFactory;
use Magento\Checkout\Model\Session;
use Magento\Framework\Session\SessionManagerInterface;
use Magento\Framework\App\Http\Context as httpContext;
use Magento\Framework\App\Cache\TypeListInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Psr\Log\LoggerInterface;

/**
 * Class Data
 */
class Data extends AbstractHelper
{

    protected $registry = ['current_product','product'];

    /**
     * @var StoreManagerInterface
     * @since 101.0.0
     */
    protected $_storeManager;

    /**
     * @var MagentoData
     * @since 101.0.0
     */
    protected $_directoryHelper;

    /**
     * @var \Magento\Framework\Registry
     * @since 101.0.0
     */
    protected $_registry;

    /**
     * @var ProductFactory
     */
    protected $_productLoader;

    /**
     * @var LoggerInterface
     */
    protected $_logger;

    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * Session entity
     *
     * @var \Magento\Framework\Session\SessionManagerInterface
     */
    protected $_session;

    /**
     * @var httpContext
     */
    protected $_httpContext;

    /**
     * @var System
     */
    private $system;

    /**
     * Data constructor.
     * @param StoreManagerInterface $storeManager
     * @param MagentoData $directoryHelper
     * @param Context $context
     * @param Registry $registry
     * @param ProductFactory $_productLoader
     * @param LoggerInterface $logger
     * @param Session $checkoutSession
     * @param SessionManagerInterface $session
     * @param httpContext $httpContext
     * @param TypeListInterface $cacheTypeList
     * @param Pool $cacheFrontendPool
     * @param System $system
     */
    public function __construct(
        StoreManagerInterface $storeManager,
        MagentoData $directoryHelper,
        Context $context,
        Registry $registry,
        ProductFactory $_productLoader,
        LoggerInterface $logger,
        Session $checkoutSession,
        SessionManagerInterface $session,
        httpContext $httpContext,
        TypeListInterface $cacheTypeList,
        Pool $cacheFrontendPool,
        System $system
    ) {
        parent::__construct($context);
        $this->_directoryHelper = $directoryHelper;
        $this->_storeManager = $storeManager;
        $this->_registry = $registry;
        $this->_productLoader = $_productLoader;
        $this->_logger = $logger;
        $this->_checkoutSession = $checkoutSession;
        $this->_session = $session;
        $this->_httpContext = $httpContext;
        $this->system = $system;
    }

    /**
     * @return bool
     */
    public function isMultishippingCheckoutAvailable() : bool
    {
        $quote = $this->getQuote();
        $isMultiShipping = $this->system->isMultiShipping();
        if (!$quote || !$quote->hasItems()) {
            return $isMultiShipping;
        }
        return $isMultiShipping && !$quote->hasItemsWithDecimalQty() && $quote->validateMinimumAmount(
                true
            ) &&
            $quote->getItemsSummaryQty() - $quote->getItemVirtualQty() > 0 &&
            $quote->getItemsSummaryQty() <= $this->system->getMaximumQty();
    }

    /**
     * @return array[]
     */
    public function getStores() : array
    {
        $stores = [
            [
                'label' => __('All Stores') . ' [' . $this->_directoryHelper->getBaseCurrencyCode() . ']',
                'value' => 0,
            ]
        ];
        $storeManagerDataList = $this->_storeManager->getStores();
        foreach ($storeManagerDataList as $key => $value) {
            $stores[] = [
                'label' => $value['name'].' - '.$value['code'],
                'value' => $key
            ];
        }
        return $stores;
    }

    /**
     * Retrieve default value for website
     *
     * @return int
     * @since 101.0.0
     */
    public function getDefaultStore() : int
    {
        return 0;
    }

    /**
     * Get Store Code
     *
     * @return string
     */
    public function getStoreCode() : string
    {
        return $this->_storeManager->getStore()->getCode();
    }

    /**
     * Get Store Int
     *
     * @return int
     */
    public function getStoreId() : int
    {
        return (int)$this->_storeManager->getStore()->getId();
    }

    /**
     * @return Product
     */
    public function getCurrentProduct() : Product
    {
        return $this->_registry->registry('current_product');
    }

    /**
     * @param $product
     */
    public function setCurrentProduct($product)
    {
        foreach ($this->registry as $registry) {
            $value = $this->_registry->registry($registry);
            if (!is_null($value)) {
                $this->_registry->unregister($registry);
            }
            try {
                $this->_registry->register($registry, $product);
            } catch (\Exception $e)
            {
                $this->_logger->critical('Current product not updated: ', ['exception' => $e]);
            }
        }
    }

    /**
     * @return Magento\Catalog\Model\ProductFactory
     */
    public function getProducById($id)
    {
        return $this->_productLoader->create()->load($id);
    }

    public function getStoreById($id)
    {
        return $this->_storeManager->getStore($id);
    }

    /**
     * Change Store
     *
     * @return void
     */
    public function changeStore($id)
    {
        if ((int)$this->getStoreId() === (int)$id) {
            return;
        }
        $this->setStore($id);
    }

    /**
     * Set Store
     *
     * @return void
     */
    public function setStore($id)
    {
        $this->_storeManager->setCurrentStore($id);
    }

    /**
     * @param bool $skipBaseNotAllowed
     * @return \Magento\Store\Api\Data\WebsiteInterface[]
     */
    public function getAvailableCurrencyCodes($skipBaseNotAllowed = false)
    {
        return $this->getWebsites();
    }

    /**
     * Retrieve checkout quote
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote()
    {
        return $this->_checkoutSession->getQuote();
    }

    /**
     * @return \Magento\Store\Api\Data\WebsiteInterface[]
     */
    public function getWebsites() {
        return $this->_storeManager->getWebsites();
    }

    /**
     * @return \Magento\Store\Api\Data\WebsiteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWebsite()
    {
        return $this->_storeManager->getWebsite();
    }

    /**
     * @param int $storeId
     * @return \Magento\Store\Api\Data\WebsiteInterface
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getWebsiteByStoreId(int $storeId)
    {
        $websiteCode = null;
        $websiteId = (int)$this->_storeManager->getStore($storeId)->getWebsiteId();
        $website = $this->_storeManager->getWebsite($websiteId);
        return $website;
    }
}