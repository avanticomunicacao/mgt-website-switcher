<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Store\Model\ScopeInterface;

/**
 * Class System
 */
class System extends AbstractHelper
{
    /**
     * @return mixed
     */
    public function isEnabled() : bool
    {
        return (bool)$this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_ENABLED,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function shareQuote() : bool
    {
        return (bool)$this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_SHARE_QUOTE,
            ScopeInterface::SCOPE_STORE
        );
    }

    public function isMultiShipping() : bool
    {
        return (bool) $this->scopeConfig->isSetFlag(
            Config::XML_PATH_WEBSITE_SWITCHER_CHECKOUT_MULTIPLE_AVAILABLE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function isCheapestStoreEnabled() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_CHEAPEST_STORE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isUpdateCart() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_UPDATE_CART,
            ScopeInterface::SCOPE_STORE
        );
    }
    /**
     * @return bool
     */
    public function isClearCart() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_CLEAR_CART,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function isEnabledChangeWebsite() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_CHANGE_WEBSITE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function successPage() : string
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_SUCCESS_PAGE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return false|string[]
     */
    public function changeWebsiteLocal() : array
    {
        $value = $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_CHANGE_WEBSITE_LOCAL,
            ScopeInterface::SCOPE_STORE
        );
        if (is_null($value)) {
            return [];
        }
        return explode(',',$value);
    }

    /**
     * @return mixed
     */
    public function showModal() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_SHOW_MODAL,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return mixed
     */
    public function updateCartInAdd() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_UPDATE_ADD_PRODUCT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function titleModal() : string
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_MODAL_TITLE,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function warningModalText() : string
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_WARNING_MODAL,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function acceptedModalTextButton() : string
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_MODAL_CONFIRM_TEXT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function declineModalTextButton() : string
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_MODAL_DECLINE_TEXT,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * Get maximum quantity allowed for shipping to multiple addresses
     *
     * @return int
     */
    public function getMaximumQty() : int
    {
        return (int)$this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_CHECKOUT_MULTIPLE_MAXIMUM_QUANTITY,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return int
     */
    public function cookieDuration() : int
    {
        return (int) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_COOKIE_DURATION,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return string
     */
    public function cookieName() : string
    {
        return $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_COOKIE_NAME,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return bool
     */
    public function isSplitOrder() : bool
    {
        return (bool) $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_SPLIT_ORDER,
            ScopeInterface::SCOPE_STORE
        );
    }

    /**
     * @return false|string[]
     */
    public function shippingInformation() : array
    {
        $value = $this->scopeConfig->getValue(
            Config::XML_PATH_WEBSITE_SWITCHER_CHECKOUT_MULTIPLE_INFORMATION,
            ScopeInterface::SCOPE_STORE
        );
        if (is_null($value)) {
            return [];
        }
        return explode(',',$value);
    }
}