<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\ViewModel\Checkout;

use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Session\StorageInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Checkout\Model\Session as MageSession;

/**
 * Class Session
 *
 * @package Avanti\WebsiteSwitcher\ViewModel\Checkout\Session
 */
class Session implements ArgumentInterface
{
    /**
     * Storage
     *
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var QuoteFactory
     */
    private $quoteFactory;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var $actualStore
     */
    private $actualStore;

    /**
     * @var $actualQuoteId
     */
    private $actualQuoteId;

    /**
     * @var MageSession
     */
    private $_checkoutSession;

    /**
     * @var System
     */
    private $system;

    /**
     * Session constructor.
     * @param StorageInterface $storage
     * @param StoreManagerInterface $_storeManager
     * @param QuoteFactory $quoteFactory
     * @param Data $data
     * @param MageSession $session
     * @param System $system
     */
    public function __construct(
        StorageInterface $storage,
        StoreManagerInterface $_storeManager,
        QuoteFactory $quoteFactory,
        Data $data,
        MageSession $session,
        System $system
    ) {
        $this->storage = $storage;
        $this->_storeManager = $_storeManager;
        $this->quoteFactory = $quoteFactory;
        $this->data = $data;
        $this->actualStore = $this->data->getStoreId();
        $this->_checkoutSession = $session;
        $this->actualQuoteId = $this->_checkoutSession->getQuoteId();
        $this->system = $system;
    }

    /**
     * @return bool|array
     */
    public function getAllQuotes()
    {
        $quotes = $this->storage->getData(Config::ALLL_QUOTE_ID);

        if (isset($quotes)) {
            return $quotes;
        }
        return false;
    }

    /**
     * @return bool
     */
    public function haveQuotes() : bool
    {
        if ($this->system->shareQuote()) {
            return false;
        }
        $values = [];
        foreach ($this->getAllQuotes() as $storeId => $quoteId) {
            if (!in_array($quoteId,$values)) {
                $values[] = $quoteId;
            }
        }
        if (count($values) > 1) {
            return true;
        }
        return false;
    }

    /**
     * @param $id
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getStoreName($id) : string
    {
        return $this->_storeManager->getStore($id)->getName();
    }

    /**
     * @param $id
     */
    public function setStore($id)
    {
        $this->data->changeStore($id);
    }

    /**
     * @return int
     */
    public function actualStore()
    {
        return $this->actualStore;
    }

    /**
     * @param $id
     */
    public function setQuoteId($id)
    {
        $this->_checkoutSession->setQuoteId($id);
    }

    /**
     * @return int
     */
    public function actualQuoteId()
    {
        return $this->actualQuoteId;
    }
}