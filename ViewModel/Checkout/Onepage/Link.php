<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\ViewModel\Checkout\Onepage;

use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\System;
use Avanti\WebsiteSwitcher\Model\Config\Source\SuccessPage;
use Magento\Checkout\Model\Cart;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Element\Template;

/**
 * Class Link
 *
 * @package Avanti\WebsiteSwitcher\ViewModel\Checkout\Onepage\Link
 */
class Link extends Template implements ArgumentInterface
{

    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;

    /**
     * @var Cart
     */
    private $cart;

    /**
     * @var System
     */
    private $system;

    /**
     * Link constructor.
     * @param Template\Context $context
     * @param QuoteHandlerInterface $quoteHandler
     * @param Cart $cart
     * @param System $system
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        QuoteHandlerInterface $quoteHandler,
        Cart $cart,
        System $system,
        array $data = []
    ) {
        parent::__construct($context, $data);
        $this->quoteHandler = $quoteHandler;
        $this->cart = $cart;
        $this->system = $system;
    }

    /**
     * @return array|bool
     */
    public function isSplitOrder()
    {
        return $this->quoteHandler->splitQuotes($this->cart->getQuote());
    }

    /**
     * @return bool|mixed
     */
    public function isEnable() : bool
    {
        return ($this->system->isEnabled() && $this->system->isMultiShipping() && $this->system->successPage() === SuccessPage::STANDART);
    }

    /**
     * @return string
     */
    public function getShippingUrl() : string
    {
        return $this->getUrl('websiteswitcher/checkout/');
    }
}