<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\ViewModel\Sales\Order;

use Magento\Framework\Registry;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Framework\View\Element\Template\Context as TemplateContext;
use Magento\Payment\Helper\Data as PaymentHelper;
use Magento\Sales\Block\Order\Info as MageInfo;
use Avanti\WebsiteSwitcher\Block\Checkout\Success;
use Magento\Sales\Model\Order\Address\Renderer as AddressRenderer;

/**
 * Class Items
 *
 * @package Avanti\WebsiteSwitcher\ViewModel\Sales\Order\Info
 */
class Info extends MageInfo implements ArgumentInterface
{
    /**
     * @var Success
     */
    private $success;

    public function __construct(
        TemplateContext $context,
        Registry $registry,
        PaymentHelper $paymentHelper,
        AddressRenderer $addressRenderer,
        Success $success,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $registry,
            $paymentHelper,
            $addressRenderer,
            $data
        );
        $this->success = $success;
    }

    /**
     * @return array|bool
     */
    public function getOrderArray()
    {
        if ($this->success->getOrderArray()) {
            foreach ($this->success->getOrderArray() as $order => $incremental) {
                $orders[] = $this->success->getOrderByIcrementId((int)$incremental);
            }
            return $orders;
        }
        return false;
    }
}
