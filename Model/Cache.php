<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model;

use Avanti\WebsiteSwitcher\Api\CacheInterface;
use Magento\Framework\App\Cache\Frontend\Pool;
use Magento\Framework\App\Cache\TypeListInterface;

/**
 * Class Cache
 */
class Cache implements CacheInterface
{

    /**
     * @var TypeListInterface
     */
    private $_cacheTypeList;

    /**
     * @var Pool
     */
    private $_cacheFrontendPool;

    /**
     * @var $cacheType
     */
    private $cacheType = [];

    /**
     * Cache constructor.
     * @param TypeListInterface $_cacheTypeList
     * @param Pool $_cacheFrontendPool
     */
    public function __construct(
        TypeListInterface $_cacheTypeList,
        Pool $_cacheFrontendPool
    ) {
        $this->_cacheTypeList = $_cacheTypeList;
        $this->_cacheFrontendPool = $_cacheFrontendPool;
        $this->initCacheTypes();
    }

    /**
     * @param array|null $types
     */
    public function clearCache(array $types = null)
    {
        foreach ($this->_cacheFrontendPool as $cacheFrontend) {
            $cacheFrontend->getBackend()->clean();
        }
        if (is_null($types)) {
            foreach ($this->cacheType as $type) {
                $this->_cacheTypeList->cleanType($type);
            }
            return;
        }
        foreach ($types as $type) {
            if (in_array($type,$this->cacheType)) {
                $this->_cacheTypeList->cleanType($type);
            }
        }
    }

    private function initCacheTypes()
    {
        foreach ($this->_cacheTypeList->getTypeLabels() as $key => $value) {
            array_push($this->cacheType,$key);
        }
    }
}