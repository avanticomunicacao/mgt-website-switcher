<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model;

use Avanti\WebsiteSwitcher\Helper\System;
use Avanti\WebsiteSwitcher\Plugin\Checkout\Model\PaymentInformationManagement;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Quote\Model\Quote;
use Magento\Quote\Api\CartManagementInterface;
use Magento\Customer\Api\Data\GroupInterface;
use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\Data;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\AddressFactory;
use Magento\Quote\Model\Quote\Item;
use Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor;
use Magento\Store\Model\Information;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class QuoteHandler
 * Responsible to build some methods from the quote.
 */
class QuoteHandler implements QuoteHandlerInterface
{

    private $storeId;

    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var Data
     */
    private $helperData;

    /**
     * @var System
     */
    private $system;

    /**
     * @var CustomerSession
     */
    private $_customerSession;

    /**
     * @var AddressRepositoryInterface
     */
    private $_addressRepository;

    /**
     * @var AddressFactory
     */
    private $_addressFactory;

    /**
     * @var Information
     */
    private $_storeInfo;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;
    /**
     * @var FilterBuilder
     */
    private $filterBuilder;
    /**
     * @var Json
     */
    private $json;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ShippingAssignmentProcessor
     */
    private $shippingAssignmentProcessor;

    /**
     * @var ManagerInterface
     */
    private $messageManager;

    /**
     * QuoteHandler constructor.
     * @param CheckoutSession $checkoutSession
     * @param Data $helperData
     * @param System $system
     * @param CustomerSession $_customerSession
     * @param AddressRepositoryInterface $addressRepository
     * @param AddressFactory $addressFactory
     * @param Information $_storeInfo
     * @param StoreManagerInterface $storeManager
     * @param SearchCriteriaBuilder $searchCriteriaBuilder
     * @param FilterBuilder $filterBuilder
     * @param Json $json
     * @param LoggerInterface $logger
     */
    public function __construct(
        CheckoutSession $checkoutSession,
        Data $helperData,
        System $system,
        CustomerSession $_customerSession,
        AddressRepositoryInterface $addressRepository,
        AddressFactory $addressFactory,
        Information $_storeInfo,
        StoreManagerInterface $storeManager,
        SearchCriteriaBuilder $searchCriteriaBuilder,
        FilterBuilder $filterBuilder,
        Json $json,
        LoggerInterface $logger,
        ManagerInterface $messageManager
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->helperData = $helperData;
        $this->system = $system;
        $this->_customerSession = $_customerSession;
        $this->_addressRepository = $addressRepository;
        $this->storeId = $this->helperData->getStoreId();
        $this->_addressFactory = $addressFactory;
        $this->_storeInfo = $_storeInfo;
        $this->_storeManager = $storeManager;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->json = $json;
        $this->logger = $logger;
        $this->messageManager = $messageManager;
    }

    /**
     * @inheritdoc
     * @param \Magento\Quote\Model\Quote $quote
     */
    public function splitQuotes(Quote $quote)
    {
        if (!$this->system->isEnabled() && !$this->system->isSplitOrder()) {
            return false;
        }
        $stores = [];
        foreach ($quote->getAllVisibleItems() as $item) {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $item->getProduct();
            $storeId = $product->getStoreId();
            if ($storeId === false || !isset($storeId)) {
                return false;
            }
            $stores[$storeId][] = $item;
        }
        if (count($stores) > 1 || !array_key_exists($this->helperData->getStoreId(),$stores)) {
            return $stores;
        }
        return false;
    }

    /**
     * @param Quote $quote
     * @return array
     */
    public function collectAddressesData(Quote $quote)
    {
        $isMultiShipping = false;
        $billing = $quote->getBillingAddress()->getData();
        unset($billing['id']);
        unset($billing['quote_id']);
        $shipping = $quote->getShippingAddress()->getData();
        unset($shipping['id']);
        unset($shipping['quote_id']);
        if ($quote->getIsMultiShipping()) {
            $isMultiShipping = true;
            foreach ($quote->getAllShippingAddresses() as $shippingAddress) {
                $storeId = $this->getShippingAddressStoreId($shippingAddress);
                $shippingAddress = $shippingAddress->getData();
                $shippingAddress['store_id'] = $storeId;
                unset($shippingAddress['id']);
                unset($shippingAddress['quote_id']);
                $shippingAddresses[] = $shippingAddress;
            }
            $shipping = $shippingAddresses;
        }
        return [
            'payment' => $quote->getPayment()->getMethod(),
            'billing' => $billing,
            'shipping' => $shipping,
            'is_multi_shipping' => $isMultiShipping
        ];
    }

    /**
     * @param $shippingAddress
     * @return mixed
     */
    public function getShippingAddressStoreId($shippingAddress)
    {
        foreach ($shippingAddress->getAllVisibleItems() as $item) {
            if ($item->getStoreId()) {
                return $item->getStoreId();
            }
        }
    }

    /**
     * @param Quote $quote
     * @param Quote $split
     * @return $this|\Magestat\SplitOrder\Api\QuoteHandlerInterface
     */
    public function setCustomerData(Quote $quote, Quote $split)
    {
        $split->setStoreId($quote->getStoreId());
        $split->setCustomer($quote->getCustomer());
        $split->setCustomerIsGuest($quote->getCustomerIsGuest());
        if ($quote->getCheckoutMethod() === CartManagementInterface::METHOD_GUEST) {
            $split->setCustomerId(null);
            $split->setCustomerEmail($quote->getBillingAddress()->getEmail());
            $split->setCustomerIsGuest(true);
            $split->setCustomerGroupId(GroupInterface::NOT_LOGGED_IN_ID);
        }
        return $this;
    }

    /**
     * @param array $quotes
     * @param Quote $split
     * @param Quote\Item[] $items
     * @param array $addresses
     * @param string $paymentMethod
     * @return $this|QuoteHandlerInterface
     */
    public function populateQuote($quotes, Quote $split, $items, $addresses, $paymentMethod)
    {
        $this->setPaymentMethod($split, $addresses['payment'], $paymentMethod);
        $this->recollectTotal($quotes, $items, $split, $addresses);
        return $this;
    }

    /**
     * @param array $quotes
     * @param Quote\Item[] $items
     * @param Quote $quote
     * @param array $addresses
     * @return $this|QuoteHandlerInterface
     */
    public function recollectTotal($quotes, $items, Quote $quote, $addresses)
    {
        $tax = 0.0;
        $discount = 0.0;
        $finalPrice = 0.0;

        foreach ($items as $item) {
            $tax += $item->getData('tax_amount');
            $discount += $item->getData('discount_amount');
            $finalPrice += ($item->getPrice() * $item->getQty());
        }
        $quote->getBillingAddress()->setData($addresses['billing']);
        $shippingAddress = $addresses['shipping'];
        if ($addresses['is_multi_shipping']) {
            foreach ($shippingAddress as $shipping) {
                if ($quote->getStoreId() === $shipping['store_id']) {
                    $shippingAddress = $shipping;
                    break;
                }
            }
        }
        $quote->getShippingAddress()->setData($shippingAddress);
        $shipping = $this->shippingAmount($quotes, $quote);
        foreach ($quote->getAllAddresses() as $address) {
            // Build grand total.
            $grandTotal = (($finalPrice + $shipping + $tax) - $discount);
            $address->setBaseSubtotal($finalPrice);
            $address->setSubtotal($finalPrice);
            $address->setDiscountAmount($discount);
            $address->setTaxAmount($tax);
            $address->setBaseTaxAmount($tax);
            $address->setBaseGrandTotal($grandTotal);
            $address->setGrandTotal($grandTotal);
        }
        return $this;
    }

    /**
     * @param array $quotes
     * @param Quote $quote
     * @param float $total
     * @return float|mixed
     */
    public function shippingAmount($quotes, Quote $quote, $total = 0.0)
    {
        if ($quote->hasVirtualItems() === true) {
            return $total;
        }
        $shippingTotals = $quote->getShippingAddress()->getShippingAmount();
//        return $this->shippingInOneOrder($quote,$shippingTotals,$total);
        return $shippingTotals;
    }

    /**
     * @param Quote $quote
     * @param $shippingTotals
     * @param $total
     * @return mixed
     */
    public function shippingInOneOrder(Quote $quote,$shippingTotals, $total)
    {
        static $process = 1;
        if ($process > 1) {
            $quote->getShippingAddress()->setShippingAmount($total);
            return $total;
        }
        $process ++;
        return $shippingTotals;
    }

    /**
     * @param string $split
     * @param Quote $payment
     * @param string $paymentMethod
     * @return $this|QuoteHandlerInterface
     */
    public function setPaymentMethod($split, $payment, $paymentMethod)
    {
        $split->getPayment()->setMethod($payment);
        if (is_null($paymentMethod)) {
            $paymentMethod = $this->_getCheckout()->getQuote()->getData(PaymentInformationManagement::AVANTI_WS_PAYMENT_METHOD);
        }
        if ($paymentMethod) {
            $split->getPayment()->setQuote($split);
            $data = $paymentMethod->getData();
            $split->getPayment()->importData($data);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function defineSessions(Quote $split, $order, $orderIds)
    {
        $this->checkoutSession->setLastQuoteId($split->getId());
        $this->checkoutSession->setLastSuccessQuoteId($split->getId());
        $this->checkoutSession->setLastOrderId($order->getId());
        $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
        $this->checkoutSession->setLastOrderStatus($order->getStatus());
        $this->checkoutSession->setOrderIds($orderIds);
        return $this;
    }

    /**
     * Get quote address by customer address ID and store ID.
     *
     * @param int|string $addressId
     * @param $storeId
     * @return Address|false
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getShippingAddressByStoreCustomerAddressId($addressId, $storeId)
    {
        /** @var \Magento\Quote\Model\Quote\Address $address */
        foreach ($this->checkoutSession->getQuote()->getAllShippingAddresses() as $address) {
            if (!$address->isDeleted() &&
                $address->getAddressType() == Address::TYPE_SHIPPING &&
                $address->getCustomerAddressId() == $addressId &&
                $address->getData('store_id') === (int)$storeId
            ) {
                    return $address;
            }
        }
        return false;
    }

    public function splitQuoteAdressesses()
    {
        $shippingInfo = $this->getShippingInfo();
        if (!\is_array($shippingInfo)) {
            throw new LocalizedException(
                __('We are unable to process your request. Please, try again later.')
            );
        }
        try {
            $this->setShippingItemsInformation($shippingInfo);
            $itemsInfo = $this->collectItemsInfo($shippingInfo);
            $totalQuantity = array_sum($itemsInfo);

            $maxQuantity = $this->system->getMaximumQty();
            if ($totalQuantity > $maxQuantity) {
                throw new LocalizedException(
                    __('Maximum qty allowed for Shipping to multiple addresses is %1', $maxQuantity)
                );
            }

            $quote = $this->checkoutSession->getQuote();
            foreach ($quote->getAllVisibleItems() as $item) {
                if (isset($itemsInfo[$item->getId()])) {
                    $this->updateItemQuantity($item, $itemsInfo[$item->getId()]);
                }
            }

            if ($quote->getHasError()) {
                throw new LocalizedException(__($quote->getMessage()));
            }
        } catch (LocalizedException $e) {
            $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->jsonResponse('We are unable to process your request. Please, try again later.');
        }
    }

    /**
     * @return array
     */
    private function getShippingInfo()
    {
        $items = [];
        foreach ($this->_getCheckout()->getQuote()->getAllVisibleItems() as $item) {
            $quoteItem = [];
            $quoteItem[$item->getItemId()] = [
                'qty' => $item->getQty(),
                'address' => $this->_customerSession->getCustomer()->getDefaultShippingAddress() ? $this->_customerSession->getCustomer()->getDefaultShippingAddress()->getId() : null,
                'store_id' => $item->getStoreId()
            ];
            $items[] = $quoteItem;
        }
        return $items;
    }

    /**
     * @return Session|CheckoutSession
     */
    private function _getCheckout()
    {
        return $this->checkoutSession;
    }

    /**
     * Assign quote items to addresses and specify items qty
     *
     * Array structure:
     * array(
     *      $quoteItemId => array(
     *          'qty'       => $qty,
     *          'address'   => $customerAddressId,
     *          'store_id'  => $storeId
     *      )
     * )
     *
     * @param array $info
     * @return Quote
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Exception
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    private function setShippingItemsInformation($info)
    {
        if (is_array($info)) {
            $allQty = 0;
            $itemsInfo = [];
            foreach ($info as $itemData) {
                foreach ($itemData as $quoteItemId => $data) {
                    $allQty += $data['qty'];
                    $itemsInfo[$quoteItemId] = $data;
                }
            }

            $maxQty = $this->system->getMaximumQty();
            if ($allQty > $maxQty) {
                throw new \Magento\Framework\Exception\LocalizedException(
                    __(
                        "The maximum quantity can't be more than %1 when shipping to multiple addresses. "
                        . "Change the quantity and try again.",
                        $maxQty
                    )
                );
            }

            $quote = $this->_getCheckout()->getQuote();
            foreach ($quote->getAllShippingAddresses() as $address) {
                $quote->removeAddress($address->getId());
            }

            $quote->setIsMultiShipping(1);
            foreach ($info as $itemData) {
                foreach ($itemData as $quoteItemId => $data) {
                    $this->helperData->changeStore($data['store_id']);
                    $this->_addShippingItem($quoteItemId, $data);
                }
            }

            $this->helperData->changeStore($this->storeId);
            $this->prepareShippingAssignment($quote);

            /**
             * Delete all not virtual quote items which are not added to shipping address
             * MultishippingQty should be defined for each quote item when it processed with _addShippingItem
             */
            foreach ($quote->getAllItems() as $_item) {
                if (!$_item->getProduct()->getIsVirtual() && !$_item->getParentItem() && !$_item->getMultishippingQty()
                ) {
                    $quote->removeItem($_item->getId());
                }
            }

            foreach ($quote->getAllItems() as $_item) {
                if (!$_item->getProduct()->getIsVirtual()) {
                    continue;
                }

                if (isset($itemsInfo[$_item->getId()]['qty'])) {
                    $qty = (int)$itemsInfo[$_item->getId()]['qty'];
                    if ($qty) {
                        $_item->setQty($qty);
                        $quote->getBillingAddress()->addItem($_item);
                    } else {
                        $_item->setQty(0);
                        $quote->removeItem($_item->getId());
                    }
                }
            }
            $this->_getCheckout()->getQuote()->save();
        }
        return $this->_getCheckout()->getQuote();
    }

    /**
     * Add quote item to specific shipping address based on customer address id
     *
     * @param int $quoteItemId
     * @param array $data array('qty'=>$qty, 'address'=>$customerAddressId)
     * @return QuoteHandler
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     *@throws \Magento\Framework\Exception\LocalizedException
     */
    protected function _addShippingItem($quoteItemId, $data)
    {
        $qty = isset($data['qty']) ? (int)$data['qty'] : 1;
        $addressId = isset($data['address']) ? $data['address'] : false;
        $quoteItem = $this->_getCheckout()->getQuote()->getItemById($quoteItemId);

        if ($quoteItem) {
            /**
             * Skip item processing if qty 0
             */
            if ($qty === 0) {
                return $this;
            }
            $quoteItem->setMultishippingQty((int)$quoteItem->getMultishippingQty() + $qty);
            $quoteItem->setQty($quoteItem->getMultishippingQty());
            try {
                $address = $this->_addressRepository->getById($addressId);
            } catch (\Exception $e) {
                $this->logger->alert($e->getMessage());
            }
            if (isset($address)) {
                if (!($quoteAddress = $this->getShippingAddressByStoreCustomerAddressId($address->getId(),$quoteItem->getStoreId()))) {
                    $quoteAddress = $this->_addressFactory->create()->importCustomerAddressData($address);
                    $quoteAddress->setData('store_id',$quoteItem->getStoreId());
                    $quoteAddress->setData('store_info',$this->_storeInfo->getStoreInformationObject($this->_storeManager->getStore($data['store_id'])));
                    $this->_getCheckout()->getQuote()->addShippingAddress($quoteAddress);
                }

                $quoteAddress = $this->getShippingAddressByStoreCustomerAddressId($address->getId(),$quoteItem->getStoreId());
                $quoteAddress->setCustomerAddressId($addressId);
                $quoteAddressItem = $quoteAddress->getItemByQuoteItemId($quoteItemId);
                if ($quoteAddressItem) {
                    $quoteAddressItem->setQty((int)($quoteAddressItem->getQty() + $qty));
                } else {
                    $quoteAddress->addItem($quoteItem, $qty);
                }
                /**
                 * Require shipping rate recollect
                 */
                $quoteAddress->setCollectShippingRates((bool)$this->_getCheckout()->getQuote()->getCollectRatesFlag());
            }
            if (!isset($address)) {
                $quoteAddress = $this->_addressFactory->create();
                if ($this->getShippingAddressByStore($data['store_id'])) {
                    $quoteAddress = $this->getShippingAddressByStore($data['store_id']);
                    $quoteAddressItem = $quoteAddress->getItemByQuoteItemId($quoteItemId);
                    if ($quoteAddressItem) {
                        $quoteAddressItem->setQty((int)($quoteAddressItem->getQty() + $qty));
                    } else {
                        $quoteAddress->addItem($quoteItem, $qty);
                    }
                    $quoteAddress->setCollectShippingRates((bool)$this->_getCheckout()->getQuote()->getCollectRatesFlag());
                    $this->_getCheckout()->getQuote()->save();
                    return $this;
                }
                $quoteAddress->setData('store_id',$quoteItem->getStoreId());
                $quoteAddress->setData('store_info',$this->_storeInfo->getStoreInformationObject($this->_storeManager->getStore($data['store_id'])));
                $quoteAddressItem = $quoteAddress->getItemByQuoteItemId($quoteItemId);
                if ($quoteAddressItem) {
                    $quoteAddressItem->setQty((int)($quoteAddressItem->getQty() + $qty));
                } else {
                    $quoteAddress->addItem($quoteItem, $qty);
                }
                $quoteAddress->setCollectShippingRates((bool)$this->_getCheckout()->getQuote()->getCollectRatesFlag());
                $this->_getCheckout()->getQuote()->addShippingAddress($quoteAddress);
            }
        }
        return $this;
    }

    public function getShippingAddressByStore($storeId)
    {
        foreach ($this->_getCheckout()->getQuote()->getAllShippingAddresses() as $shippingAddress) {
            if ($shippingAddress->getData('store_id') === $storeId) {
                return $shippingAddress;
            }
        }
        return false;
    }

    /**
     * Group posted items.
     *
     * @param array $shippingInfo
     * @return array
     */
    private function collectItemsInfo(array $shippingInfo): array
    {
        $itemsInfo = [];
        foreach ($shippingInfo as $itemData) {
            if (!\is_array($itemData)) {
                continue;
            }
            foreach ($itemData as $quoteItemId => $data) {
                if (!isset($itemsInfo[$quoteItemId])) {
                    $itemsInfo[$quoteItemId] = 0;
                }
                $itemsInfo[$quoteItemId] += (double)$data['qty'];
            }
        }

        return $itemsInfo;
    }

    /**
     * Updates quote item quantity.
     *
     * @param Item $item
     * @param float $quantity
     * @throws LocalizedException
     */
    private function updateItemQuantity(Item $item, float $quantity)
    {
        if ($quantity > 0) {
            $item->setQty($quantity);
            if ($item->getHasError()) {
                throw new LocalizedException(__($item->getMessage()));
            }
        }
    }

    /**
     * Prepare shipping assignment.
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return \Magento\Quote\Model\Quote
     */
    private function prepareShippingAssignment($quote)
    {
        $cartExtension = $quote->getExtensionAttributes();
        if ($cartExtension === null) {
            $cartExtension = $this->cartExtensionFactory->create();
        }
        /** @var \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment */
        $shippingAssignment = $this->getShippingAssignmentProcessor()->create($quote);
        $shipping = $shippingAssignment->getShipping();

        $shipping->setMethod(null);
        $shippingAssignment->setShipping($shipping);
        $cartExtension->setShippingAssignments([$shippingAssignment]);
        return $quote->setExtensionAttributes($cartExtension);
    }

    /**
     * Retrieve customer default billing address
     *
     * @return int|null
     */
    public function getCustomerDefaultBillingAddress()
    {
        $defaultAddressId = $this->getCustomer()->getDefaultBilling();
        return $this->getDefaultAddressByDataKey('customer_default_billing_address', $defaultAddressId);
    }

    /**
     * Retrieve customer object
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer()
    {
        return $this->_customerSession->getCustomerDataObject();
    }

    /**
     * Retrieve customer default address by data key
     *
     * @param string $key
     * @param string|null $defaultAddressIdFromCustomer
     * @return int|null
     */
    private function getDefaultAddressByDataKey($key, $defaultAddressIdFromCustomer)
    {
        $addressId = $this->_getCheckout()->getQuote()->getData($key);
        if ($addressId === null) {
            $addressId = $defaultAddressIdFromCustomer;
            if (!$addressId) {
                /** Default address is not available, try to find any customer address */
                $filter = $this->filterBuilder->setField('parent_id')
                    ->setValue($this->getCustomer()->getId())
                    ->setConditionType('eq')
                    ->create();
                $addresses = (array)($this->_addressRepository->getList(
                    $this->searchCriteriaBuilder->addFilters([$filter])->create()
                )->getItems());
                if ($addresses) {
                    $address = reset($addresses);
                    $addressId = $address->getId();
                }
            }
            $this->_getCheckout()->getQuote()->setData($key, $addressId);
        }

        return $addressId;
    }

    /**
     * Get shipping assignment processor.
     *
     * @return ShippingAssignmentProcessor
     */
    private function getShippingAssignmentProcessor()
    {
        if (!$this->shippingAssignmentProcessor) {
            $this->shippingAssignmentProcessor = ObjectManager::getInstance()
                ->get(ShippingAssignmentProcessor::class);
        }
        return $this->shippingAssignmentProcessor;
    }

    /**
     * JSON response builder.
     *
     * @param string $error
     * @return bool|false|string
     */
    private function jsonResponse(string $error = '')
    {
        return $this->json->serialize($this->getResponseData($error));
    }

    /**
     * Returns response data.
     *
     * @param string $error
     * @return array
     */
    private function getResponseData(string $error = ''): array
    {
        $response = [
            'success' => true,
        ];

        if (!empty($error)) {
            $response = [
                'success' => false,
                'error_message' => $error,
            ];
        }

        return $response;
    }
}
