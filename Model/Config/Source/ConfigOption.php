<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

namespace Avanti\WebsiteSwitcher\Model\Config\Source;

use Magento\Framework\Option\ArrayInterface;

/**
 * Class ConfigOption
 */
class ConfigOption implements ArrayInterface
{
    /**
     * Render options
     *
     * @return array[]
     */
    public function toOptionArray()
    {
        return [
            ['value' => 'header', 'label' => __('header')],
            ['value' => 'footer', 'label' => __('footer')],
        ];
    }
}