<?php declare(strict_types=1);


namespace Avanti\WebsiteSwitcher\Model\Tax;

use Magento\Tax\Model\Sales\Total\Quote\Shipping as ShippingTax;
use Magento\Quote\Model\Quote;
use Magento\Quote\Api\Data\ShippingAssignmentInterface;
use Magento\Quote\Model\Quote\Address\Total;

class Shipping extends ShippingTax
{
    /**
     * Collect tax totals for shipping. The result can be used to calculate discount on shipping
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param ShippingAssignmentInterface $shippingAssignment
     * @param Address\Total $total
     * @return $this
     */
    public function collect(
        Quote $quote,
        ShippingAssignmentInterface $shippingAssignment,
        Total $total
    ) {
        $storeId = $quote->getStoreId();
        $items = $shippingAssignment->getItems();
        if (!$items) {
            return $this;
        }

        //Add shipping
        $shippingDataObject = $this->getShippingDataObject($shippingAssignment, $total, false);
        $baseShippingDataObject = $this->getShippingDataObject($shippingAssignment, $total, true);
        if ($shippingDataObject == null || $baseShippingDataObject == null) {
            return $this;
        }

        $quoteDetails = $this->prepareQuoteDetails($shippingAssignment, [$shippingDataObject]);
        $taxDetails = $this->taxCalculationService
            ->calculateTax($quoteDetails, $storeId);
        $taxDetailsItems = $taxDetails->getItems()[self::ITEM_CODE_SHIPPING];

        $baseQuoteDetails = $this->prepareQuoteDetails($shippingAssignment, [$baseShippingDataObject]);
        $baseTaxDetails = $this->taxCalculationService
            ->calculateTax($baseQuoteDetails, $storeId);
        $baseTaxDetailsItems = $baseTaxDetails->getItems()[self::ITEM_CODE_SHIPPING];
        
        if ($quote->getIsMultiShipping()) {
            $address = $shippingAssignment->getShipping()->getAddress();
            $address->setShippingAmount($taxDetailsItems->getRowTotal());
            $address->setBaseShippingAmount($taxDetailsItems->getRowTotal());
        } else {
            $quote->getShippingAddress()
                ->setShippingAmount($taxDetailsItems->getRowTotal());
            $quote->getShippingAddress()
                ->setBaseShippingAmount($baseTaxDetailsItems->getRowTotal());
        }

        $this->processShippingTaxInfo(
            $shippingAssignment,
            $total,
            $taxDetailsItems,
            $baseTaxDetailsItems
        );

        return $this;
    }
}