<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model\Checkout\CustomerData;

use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Helper\Image;
use Magento\Catalog\Helper\Product\ConfigurationPool;
use Magento\Catalog\Model\Product\Configuration\Item\ItemResolverInterface;
use Magento\Checkout\CustomerData\DefaultItem as MageDefaultItem;
use Magento\Framework\Escaper;
use Magento\Framework\UrlInterface;

/**
 * Default item
 */
class DefaultItem extends MageDefaultItem
{

    /**
     * @var System
     */
    private $system;

    /**
     * @var Data
     */
    private $data;
    /**
     * @var Escaper|null
     */
    private $escaper;

    public function __construct(
        Image $imageHelper,
        \Magento\Msrp\Helper\Data $msrpHelper,
        UrlInterface $urlBuilder,
        ConfigurationPool
        $configurationPool,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        Escaper $escaper,
        ItemResolverInterface $itemResolver,
        System $system,
        Data $data
    ) {
        parent::__construct(
            $imageHelper,
            $msrpHelper,
            $urlBuilder,
            $configurationPool,
            $checkoutHelper,
            $escaper,
            $itemResolver
        );
        $this->system = $system;
        $this->data = $data;
        $this->escaper = $escaper;
    }

    /**
     * {@inheritdoc}
     */
    protected function doGetItemData()
    {
        if (!$this->system->isEnabled()) {
            return parent::doGetItemData();
        }
        $imageHelper = $this->imageHelper->init($this->getProductForThumbnail(), 'mini_cart_product_thumbnail');
        $productName = $this->escaper->escapeHtml($this->item->getProduct()->getName());

        return [
            'options' => $this->getOptionList(),
            'qty' => $this->item->getQty() * 1,
            'item_id' => $this->item->getId(),
            'configure_url' => $this->getConfigureUrl(),
            'is_visible_in_site_visibility' => $this->item->getProduct()->isVisibleInSiteVisibility(),
            'product_id' => $this->item->getProduct()->getId(),
            'product_name' => $productName,
            'store_name' => $this->data->getStoreById($this->item->getProduct()->getStoreId())->getName(),
            'product_sku' => $this->item->getProduct()->getSku(),
            'product_url' => $this->getProductUrl(),
            'product_has_url' => $this->hasProductUrl(),
            'product_price' => $this->checkoutHelper->formatPrice($this->item->getCalculationPrice()),
            'product_price_value' => $this->item->getCalculationPrice(),
            'product_image' => [
                'src' => $imageHelper->getUrl(),
                'alt' => $imageHelper->getLabel(),
                'width' => $imageHelper->getWidth(),
                'height' => $imageHelper->getHeight(),
            ],
            'canApplyMsrp' => $this->msrpHelper->isShowBeforeOrderConfirm($this->item->getProduct())
                && $this->msrpHelper->isMinimalPriceLessMsrp($this->item->getProduct()),
        ];
    }
}
