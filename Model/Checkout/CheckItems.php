<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model\Checkout;

use Avanti\WebsiteSwitcher\Api\Checkout\CheckItemsInterface;
use Avanti\WebsiteSwitcher\Api\Checkout\Type\MultishippingInterface;
use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\System;
use Avanti\WebsiteSwitcher\Helper\Url;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping\State;
use Magento\Customer\Model\Session as CustomerSession;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Avanti\WebsiteSwitcher\Controller\Checkout;
use Avanti\WebsiteSwitcher\Helper\Data as MultishippingHelper;
use Magento\Quote\Model\Quote\Item;
use Psr\Log\LoggerInterface;

class CheckItems extends Checkout implements CheckItemsInterface
{
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var MultishippingHelper
     */
    private $helper;

    /**
     * @var Json
     */
    private $json;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var System
     */
    private $system;

    /**
     * @param Context $context ,
     * @param CustomerSession $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param MultishippingInterface $multishipping
     * @param CheckoutSession $checkoutSession
     * @param MultishippingHelper $helper
     * @param Json $json
     * @param LoggerInterface $logger
     * @param Url $url
     * @param State $state
     * @param System $system
     */

    public function __construct(
        Context $context,
        CustomerSession $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        MultishippingInterface $multishipping,
        CheckoutSession $checkoutSession,
        MultishippingHelper $helper,
        Json $json,
        LoggerInterface $logger,
        Url $url,
        State $state,
        System $system
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->helper = $helper;
        $this->json = $json;
        $this->logger = $logger;

        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement,
            $multishipping,
            $url,
            $checkoutSession,
            $state
        );
        $this->system = $system;
    }

    /**
     * @return void
     */
    public function execute()
    {
        try {
            $this->_getCheckout()->setCollectRatesFlag(true);
            $shippingInfo = $this->getShippingInfo();
            $this->_getCheckout()->setShippingItemsInformation($shippingInfo);
            if (!\is_array($shippingInfo)) {
                throw new LocalizedException(
                    __('We are unable to process your request. Please, try again later.')
                );
            }

            $itemsInfo = $this->collectItemsInfo($shippingInfo);
            $totalQuantity = array_sum($itemsInfo);

            $maxQuantity = $this->system->getMaximumQty();
            if ($totalQuantity > $maxQuantity) {
                throw new LocalizedException(
                    __('Maximum qty allowed for Shipping to multiple addresses is %1', $maxQuantity)
                );
            }

            $quote = $this->checkoutSession->getQuote();
            foreach ($quote->getAllItems() as $item) {
                if (isset($itemsInfo[$item->getId()])) {
                    $this->updateItemQuantity($item, $itemsInfo[$item->getId()]);
                }
            }

            if ($quote->getHasError()) {
                throw new LocalizedException(__($quote->getMessage()));
            }

        } catch (LocalizedException $e) {
            $this->jsonResponse($e->getMessage());
        } catch (\Exception $e) {
            $this->logger->critical($e->getMessage());
            $this->jsonResponse('We are unable to process your request. Please, try again later.');
        }
    }

    /**
     * @return array
     */
    public function getShippingInfo()
    {
        $items = [];
        foreach ($this->multishipping->getQuoteItems() as $item) {
            $quoteItem = [];
            $quoteItem[$item->getItemId()] = [
                'qty' => $item->getQty(),
                'address' => $this->_customerSession->getCustomer()->getDefaultShippingAddress() ? $this->_customerSession->getCustomer()->getDefaultShippingAddress()->getId() : null,
                'store_id' => $item->getStoreId()
            ];
            $items[] = $quoteItem;
        }
        return $items;
    }

    /**
     * Updates quote item quantity.
     *
     * @param Item $item
     * @param float $quantity
     * @throws LocalizedException
     */
    private function updateItemQuantity(Item $item, float $quantity)
    {
        if ($quantity > 0) {
            $item->setQty($quantity);
            if ($item->getHasError()) {
                throw new LocalizedException(__($item->getMessage()));
            }
        }
    }

    /**
     * Group posted items.
     *
     * @param array $shippingInfo
     * @return array
     */
    private function collectItemsInfo(array $shippingInfo): array
    {
        $itemsInfo = [];
        foreach ($shippingInfo as $itemData) {
            if (!\is_array($itemData)) {
                continue;
            }
            foreach ($itemData as $quoteItemId => $data) {
                if (!isset($itemsInfo[$quoteItemId])) {
                    $itemsInfo[$quoteItemId] = 0;
                }
                $itemsInfo[$quoteItemId] += (double)$data['qty'];
            }
        }

        return $itemsInfo;
    }

    /**
     * JSON response builder.
     *
     * @param string $error
     * @return void
     */
    private function jsonResponse(string $error = '')
    {
        $this->getResponse()->representJson(
            $this->json->serialize($this->getResponseData($error))
        );
    }

    /**
     * Returns response data.
     *
     * @param string $error
     * @return array
     */
    private function getResponseData(string $error = ''): array
    {
        $response = [
            'success' => true,
        ];

        if (!empty($error)) {
            $response = [
                'success' => false,
                'error_message' => $error,
            ];
        }

        return $response;
    }
}
