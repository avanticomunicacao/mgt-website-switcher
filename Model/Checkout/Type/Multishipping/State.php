<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;

use Avanti\WebsiteSwitcher\Helper\Config;
use Magento\Checkout\Model\Session;
use Magento\Framework\DataObject;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;

/**
 * Multishipping checkout state model
 */
class State extends DataObject
{
    /**
     * Allow steps array
     *
     * @var array
     */
    protected $_steps;

    /**
     * Checkout model
     *
     * @var Multishipping
     */
    protected $_multishipping;

    /**
     * @var Session
     */
    protected $_checkoutSession;

    /**
     * Init model, steps
     *
     * @param Session $checkoutSession
     * @param Multishipping $multishipping
     */
    public function __construct(Session $checkoutSession, Multishipping $multishipping)
    {
        $this->_checkoutSession = $checkoutSession;
        $this->_multishipping = $multishipping;
        parent::__construct();
        $this->_steps = [
            Config::STEP_SELECT_ADDRESSES => new DataObject(['label' => __('Select Addresses')]),
            Config::STEP_SHIPPING => new DataObject(['label' => __('Shipping Information')]),
            Config::STEP_BILLING => new DataObject(['label' => __('Billing Information')]),
            Config::STEP_OVERVIEW => new DataObject(['label' => __('Place Order')]),
            Config::STEP_SUCCESS => new DataObject(['label' => __('Order Success')]),
            Config::STEP_RESULTS => new DataObject(['label' => __('Order Results')]),
        ];

        foreach ($this->_steps as $step) {
            $step->setIsComplete(false);
        }
        $this->_steps[$this->getActiveStep()]->setIsActive(true);
    }

    /**
     * Retrieve checkout model
     *
     * @return Multishipping
     */
    public function getCheckout()
    {
        return $this->_multishipping;
    }

    /**
     * Retrieve available checkout steps
     *
     * @return array
     */
    public function getSteps()
    {
        return $this->_steps;
    }

    /**
     * Retrieve active step code
     *
     * @return string
     */
    public function getActiveStep()
    {
        $step = $this->getCheckoutSession()->getCheckoutState();
        if (isset($this->_steps[$step])) {
            return $step;
        }
        return Config::STEP_SELECT_ADDRESSES;
    }

    /**
     * @param string $step
     * @return $this
     */
    public function setActiveStep($step)
    {
        if (isset($this->_steps[$step])) {
            $this->getCheckoutSession()->setCheckoutState($step);
        } else {
            $this->getCheckoutSession()->setCheckoutState(Config::STEP_SELECT_ADDRESSES);
        }

        // Fix active step changing
        if (!$this->_steps[$step]->getIsActive()) {
            foreach ($this->getSteps() as $stepObject) {
                $stepObject->unsIsActive();
            }
            $this->_steps[$step]->setIsActive(true);
        }
        return $this;
    }

    /**
     * Mark step as completed
     *
     * @param string $step
     * @return $this
     */
    public function setCompleteStep($step)
    {
        if (isset($this->_steps[$step])) {
            $this->getCheckoutSession()->setStepData($step, 'is_complete', true);
        }
        return $this;
    }

    /**
     * Retrieve step complete status
     *
     * @param string $step
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getCompleteStep($step)
    {
        if (isset($this->_steps[$step])) {
            return $this->getCheckoutSession()->getStepData($step, 'is_complete');
        }
        return false;
    }

    /**
     * Unset complete status from step
     *
     * @param string $step
     * @return $this
     */
    public function unsCompleteStep($step)
    {
        if (isset($this->_steps[$step])) {
            $this->getCheckoutSession()->setStepData($step, 'is_complete', false);
        }
        return $this;
    }

    /**
     * @return bool
     */
    public function canSelectAddresses()
    {
    }

    /**
     * @return bool
     */
    public function canInputShipping()
    {
    }

    /**
     * @return bool
     */
    public function canSeeOverview()
    {
    }

    /**
     * @return bool
     */
    public function canSuccess()
    {
    }

    /**
     * Retrieve checkout session
     *
     * @return Session
     */
    public function getCheckoutSession()
    {
        return $this->_checkoutSession;
    }
}