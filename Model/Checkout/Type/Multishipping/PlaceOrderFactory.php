<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;

use Magento\Framework\ObjectManagerInterface;

/**
 * Creates instance of place order service according to payment provider.
 */
class PlaceOrderFactory
{
    /**
     * @var ObjectManagerInterface
     */
    private $objectManager;

    /**
     * @var PlaceOrderPool
     */
    private $placeOrderPool;

    /**
     * @param ObjectManagerInterface $objectManager
     * @param PlaceOrderPool $placeOrderPool
     */
    public function __construct(
        ObjectManagerInterface $objectManager,
        PlaceOrderPool $placeOrderPool
    ) {
        $this->objectManager = $objectManager;
        $this->placeOrderPool = $placeOrderPool;
    }

    /**
     * @param string $paymentProviderCode
     * @return PlaceOrderInterface
     */
    public function create(string $paymentProviderCode): PlaceOrderInterface
    {
        $service = $this->placeOrderPool->get($paymentProviderCode);
        if ($service === null) {
            $service = $this->objectManager->get(PlaceOrderDefault::class);
        }

        return $service;
    }
}
