<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;

use Magento\Framework\ObjectManager\TMap;
use Magento\Framework\ObjectManager\TMapFactory;

/**
 * Contains place order services according to payment provider.
 *
 * Can be used as extension point for changing order placing logic during multishipping checkout flow.
 */
class PlaceOrderPool
{
    /**
     * @var PlaceOrderInterface[] | TMap
     */
    private $services;

    /**
     * @param TMapFactory $tmapFactory
     * @param array $services
     */
    public function __construct(
        TMapFactory $tmapFactory,
        array $services = []
    ) {
        $this->services = $tmapFactory->createSharedObjectsMap(
            [
                'array' => $services,
                'type' => PlaceOrderInterface::class
            ]
        );
    }

    /**
     * Returns place order service for defined payment provider.
     *
     * @param string $paymentProviderCode
     * @return PlaceOrderInterface|null
     */
    public function get(string $paymentProviderCode)
    {
        if (!isset($this->services[$paymentProviderCode])) {
            return null;
        }

        return $this->services[$paymentProviderCode];
    }
}
