<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Quote\Api\Data\CartInterfaceFactory;
use Magento\Quote\Api\Data\CartSearchResultsInterfaceFactory;
use Magento\Quote\Model\QuoteFactory;
use Magento\Quote\Model\QuoteRepository as MageQuoteRepository;
use Magento\Quote\Model\ResourceModel\Quote\Collection as QuoteCollection;
use Magento\Quote\Model\ResourceModel\Quote\CollectionFactory as QuoteCollectionFactory;
use Magento\Store\Model\StoreManagerInterface;

/**
 * Class QuoteRepository
 */
class QuoteRepository extends MageQuoteRepository
{

    /**
     * @var Data
     */
    private $data;
    /**
     * @var System
     */
    private $system;

    /**
     * QuoteRepository constructor.
     * @param QuoteFactory $quoteFactory
     * @param StoreManagerInterface $storeManager
     * @param QuoteCollection $quoteCollection
     * @param CartSearchResultsInterfaceFactory $searchResultsDataFactory
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param CollectionProcessorInterface|null $collectionProcessor
     * @param QuoteCollectionFactory|null $quoteCollectionFactory
     * @param CartInterfaceFactory|null $cartFactory
     * @param System $system
     */
    public function __construct(
        QuoteFactory $quoteFactory,
        StoreManagerInterface $storeManager,
        QuoteCollection $quoteCollection,
        CartSearchResultsInterfaceFactory $searchResultsDataFactory,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        CollectionProcessorInterface $collectionProcessor = null,
        QuoteCollectionFactory $quoteCollectionFactory = null,
        CartInterfaceFactory $cartFactory = null,
        System $system
    ) {
        parent::__construct(
            $quoteFactory,
            $storeManager,
            $quoteCollection,
            $searchResultsDataFactory,
            $extensionAttributesJoinProcessor,
            $collectionProcessor,
            $quoteCollectionFactory,
            $cartFactory
        );
        $this->system = $system;
    }

    /**
     * Load quote with different methods
     * Will consider all stores valid, quote can be shared among all stores.
     *
     * @param string $loadMethod
     * @param string $loadField
     * @param int $identifier
     * @param int[] $sharedStoreIds
     * @return \Magento\Quote\Api\Data\CartInterface
     * @throws NoSuchEntityException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    protected function loadQuote($loadMethod, $loadField, $identifier, array $sharedStoreIds = [])
    {
        if ($this->system->isEnabled() && $this->system->shareQuote()) {
            $sharedStoreIds = $this->getStoreIds();
            /** @var Quote $quote */
            $quote = $this->quoteFactory->create();
            if ($sharedStoreIds) {
                $quote->setSharedStoreIds($sharedStoreIds);
            }
            $quote->setStoreId($this->storeManager->getStore()->getId())->$loadMethod($identifier);
            if (!$quote->getId()) {
                throw NoSuchEntityException::singleField($loadField, $identifier);
            }
            return $quote;
        }
        return parent::loadQuote($loadMethod, $loadField, $identifier, $sharedStoreIds);
    }


    /**
     * @return int[]
     */
    protected function getStoreIds()
    {
        $storeIds = [];
        $stores = $this->storeManager->getStores();

        foreach ($stores as $storeId => $store) {
            $storeIds[] = $storeId;
        }
        return $storeIds;
    }
}
