<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model;

use Avanti\WebsiteSwitcher\Api\CookieInterface;
use Exception;
use Avanti\WebsiteSwitcher\Api\CartInterface;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Model\ProductFactory;
use Magento\CatalogInventory\Api\StockStateInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\Store\Api\WebsiteRepositoryInterface;
use Magento\Store\Model\StoreManagerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class Cart
 */
class Cart implements CartInterface
{

    /**
     * @var System
     */
    private $system;

    /**
     * @var \Magento\Checkout\Helper\Cart
     */
    private $_cart;

    /**
     * @var ProductFactory
     */
    private $_productFactory;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var WebsiteRepositoryInterface
     */
    private $_websiteRepository;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var StockStateInterface
     */
    private $_stockState;

    /**
     * @var ManagerInterface
     */
    private $_messageManager;

    /**
     * @var CookieInterface
     */
    private $cookie;

    /**
     * Cart constructor.
     * @param System $system
     * @param \Magento\Checkout\Helper\Cart $_cart
     * @param ProductFactory $_productFactory
     * @param LoggerInterface $logger
     * @param WebsiteRepositoryInterface $_websiteRepository
     * @param StoreManagerInterface $_storeManager
     * @param StockStateInterface $_stockState
     * @param ManagerInterface $_messageManager
     * @param CookieInterface $cookie
     */
    public function __construct(
        System $system,
        \Magento\Checkout\Helper\Cart $_cart,
        ProductFactory $_productFactory,
        LoggerInterface $logger,
        WebsiteRepositoryInterface $_websiteRepository,
        StoreManagerInterface $_storeManager,
        StockStateInterface $_stockState,
        ManagerInterface $_messageManager,
        CookieInterface $cookie
    ) {
        $this->system = $system;
        $this->_cart = $_cart;
        $this->_productFactory = $_productFactory;
        $this->logger = $logger;
        $this->_websiteRepository = $_websiteRepository;
        $this->_storeManager = $_storeManager;
        $this->_stockState = $_stockState;
        $this->_messageManager = $_messageManager;
        $this->cookie = $cookie;
    }

    /**
     * @param string $code
     * @throws Exception
     */
    public function updateCart(string $code = null)
    {
        if ($this->_cart->getCart() && $this->system->isUpdateCart()) {
            if (!is_null($code)) {
                $quoteStoreId = $this->getStoreIdByWebsiteId($this->getWebsiteId($code));
            }
            $quote = $this->_cart->getQuote();
            if (is_null($code)) {
                $quoteStoreId = $quote->getStoreId();
            }
            if ($this->checkItems($quote)) {
                foreach ($quote->getItems() as $item) {
                    if ($this->system->isClearCart() || !$this->_stockState->verifyStock((int) $item->getData('product_id'),$this->getWebsiteId($code))) {
                        $this->_cart->getQuote()->removeItem($item->getId());
                        continue;
                    }
                    $item->setStoreId($quoteStoreId);
                    $product = $this->_productFactory->create()
                        ->setStoreId($item->getStoreId())
                        ->load($item->getData('product_id'));
                    $item->setProduct($product);
                    $items[] = $item;
                }
                if (isset($items)) {
                    $this->_cart->getQuote()
                        ->setItems($items);
                }
                $this->_cart->getQuote()->collectTotals()->save();
            }
        }
    }

    /**
     * @param $quote
     * @return bool
     */
    public function checkItems($quote)
    {
        return ($quote->hasItems() && !empty($quote->getItems()));
    }

    /**
     * get website id
     *
     * @param string $code
     * @return int|null
     */
    public function getWebsiteId(string $code)
    {
        $websiteId = null;
        try {
            $website = $this->_websiteRepository->get($code);
            $websiteId = (int)$website->getId();
        } catch (Exception $exception) {
            $this->logger->error($exception->getMessage());
        }
        return $websiteId;
    }

    /**
     * Get store id by website id
     *
     * @param int $websiteId
     * @return id|int
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws Exception
     */
    public function getStoreIdByWebsiteId(int $websiteId)
    {
        if (is_null($this->_storeManager->getWebsite($websiteId)->getDefaultStore())) {
            $message = __('Something went wrong with change website.');
            $this->_messageManager->addError($message);
            $this->logger->error('website change error: dont have store associate');
            $this->cookie->set($this->getWebsiteCodeById((int) $this->_storeManager->getDefaultStoreView()->getWebsiteId()));
            return $this->_storeManager->getDefaultStoreView()->getWebsiteId();
        }
        return $this->_storeManager->getWebsite($websiteId)
            ->getDefaultStore()
            ->getId();
    }

    /**
     * @param $productId
     * @param null $websiteId
     * @return mixed
     */
    public function getStockQty(int $productId,int $websiteId = null)
    {
        return $this->_stockState->getStockQty($productId, $websiteId);
    }

    /**
     * @param $websiteId
     * @return string
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getWebsiteCodeById(int $websiteId) : string
    {
        return $this->_storeManager->getWebsite($websiteId)->getCode();
    }
}