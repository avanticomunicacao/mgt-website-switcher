<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Model;

use Avanti\WebsiteSwitcher\Api\InventoryInterface;
use Exception;
use Magento\InventoryApi\Api\SourceRepositoryInterface;
use Magento\InventoryApi\Api\StockRepositoryInterface;
use Magento\Store\Model\Website;
use Psr\Log\LoggerInterface;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\InventoryApi\Api\GetStockSourceLinksInterface;
use Magento\InventoryApi\Api\Data\StockSourceLinkInterface;

/**
 * Class Inventory
 */
class Inventory implements InventoryInterface
{
    /**
     * @var SearchCriteriaBuilder
     */
    private $searchCriteriaBuilder;

    /**
     * @var GetStockSourceLinksInterface
     */
    private $getStockSourceLinks;

    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var StockRepositoryInterface
     */
    private $stockRepository;
    /**
     * @var SourceRepositoryInterface
     */
    private $sourceRepository;

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        GetStockSourceLinksInterface $getStockSourceLinks,
        LoggerInterface $logger,
        StockRepositoryInterface $stockRepository,
        SourceRepositoryInterface $sourceRepository
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->getStockSourceLinks = $getStockSourceLinks;
        $this->logger = $logger;
        $this->stockRepository = $stockRepository;
        $this->sourceRepository = $sourceRepository;
    }

    /**
     * Retrieves links that are assigned to $stockId
     *
     * @param int $stockId
     * @return StockSourceLinkInterface[]
     */
    public function getAssignedSource(int $stockId)
    {
        $searchCriteria = $this->searchCriteriaBuilder
            ->addFilter(StockSourceLinkInterface::STOCK_ID, $stockId)
            ->create();
        $result = [];
        foreach ($this->getStockSourceLinks->execute($searchCriteria)->getItems() as $link) {
            $result[$link->getSourceCode()] = $link;
        }
        return $link;
    }

    /**
     * @param Website $website
     * @return bool|\Magento\InventoryApi\Api\Data\StockInterface|mixed
     */
    public function getStockByWebsite(Website $website)
    {
        $stocks = $this->stockRepository->getList();
        foreach ($stocks->getItems() as $stock) {
            foreach ($stock->getExtensionAttributes()->getSalesChannels() as $salesChannel) {
                if ($salesChannel->getCode() === $website->getCode()) {
                    return $stock;
                }
            }
        }
        return false;
    }

    /**
     * @param $sourceCode
     * @return string
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getSourceAddress($sourceCode)
    {
        $source = $this->sourceRepository->get($sourceCode);
        $address = [
            $source->getName(),
            $source->getCity(),
            $source->getStreet(),
            $source->getCountryId(),
            $source->getPostcode(),
            "T: ".$source->getPhone()
        ];
        $html = "";
        foreach ($address as $item) {
            if (!empty($item)) {
                $html .= $item."</br>";
            }
        }
        return $html;
    }
}