<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Api\Checkout\Type;

/**
 * Interface MultishippingInterface
 * @api
 */
interface MultishippingInterface
{
    /**
     * Get quote items assigned to different quote addresses populated per item qty.
     *
     * Based on result array we can display each item separately
     *
     * @return array
     */
    public function getQuoteShippingAddressesItems();

    /**
     * Remove item from address
     *
     * @param int $addressId
     * @param int $itemId
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     */
    public function removeAddressItem($addressId, $itemId);

    /**
     * Assign quote items to addresses and specify items qty
     *
     * Array structure:
     * array(
     *      $quoteItemId => array(
     *          'qty'       => $qty,
     *          'address'   => $customerAddressId
     *      )
     * )
     *
     * @param array $info
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     * @throws \Magento\Framework\Exception\LocalizedException
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @SuppressWarnings(PHPMD.NPathComplexity)
     */
    public function setShippingItemsInformation($info);

    /**
     * Reimport customer address info to quote shipping address
     *
     * @param int $addressId customer address id
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     */
    public function updateQuoteCustomerShippingAddress($addressId);

    /**
     * Reimport customer billing address to quote
     *
     * @param int $addressId customer address id
     * @throws \Magento\Framework\Exception\LocalizedException
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     */
    public function setQuoteCustomerBillingAddress($addressId);

    /**
     * Assign shipping methods to addresses
     *
     * @param  array $methods
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function setShippingMethods($methods);

    /**
     * Set payment method info to quote payment
     *
     * @param array $payment
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function setPaymentMethod($payment);

    /**
     * Create orders per each quote address
     *
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     * @throws \Exception
     */
    public function createOrders();

    /**
     * Collect quote totals and save quote object
     *
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     */
    public function save();

    /**
     * Specify BEGIN state in checkout session whot allow reinit multishipping checkout
     *
     * @return \Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping
     */
    public function reset();

    /**
     * Check if quote amount is allowed for multishipping checkout
     *
     * @return bool
     */
    public function validateMinimumAmount();

    /**
     * Get notification message for case when multishipping checkout is not allowed
     *
     * @return string
     */
    public function getMinimumAmountDescription();

    /**
     * Get minimum amount error.
     *
     * @return string
     */
    public function getMinimumAmountError();

    /**
     * Get order IDs created during checkout
     *
     * @param bool $asAssoc
     * @return array
     */
    public function getOrderIds($asAssoc = false);

    /**
     * Retrieve customer default billing address
     *
     * @return int|null
     */
    public function getCustomerDefaultBillingAddress();

    /**
     * Retrieve customer default shipping address
     *
     * @return int|null
     */
    public function getCustomerDefaultShippingAddress();

    /**
     * Retrieve checkout session model
     *
     * @return Session
     */
    public function getCheckoutSession();

    /**
     * Retrieve quote model
     *
     * @return \Magento\Quote\Model\Quote
     */
    public function getQuote();

    /**
     * Retrieve quote items
     *
     * @return \Magento\Quote\Model\Quote\Item[]
     */
    public function getQuoteItems();

    /**
     * Retrieve customer session model
     *
     * @return \Magento\Customer\Model\Session
     */
    public function getCustomerSession();

    /**
     * Retrieve customer object
     *
     * @return \Magento\Customer\Api\Data\CustomerInterface
     */
    public function getCustomer();
}