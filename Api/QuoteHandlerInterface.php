<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Api;

use Magento\Quote\Model\Quote;

/**
 * Interface QuoteHandlerInterface
 * @api
 */
interface QuoteHandlerInterface
{
    /**
     * Separate all items in quote into new quotes.
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return bool|array False if not split, or an array of array of split items
     */
    public function splitQuotes(Quote $quote);

    /**
     * Collect list of data addresses.
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return array
     */
    public function collectAddressesData(Quote $quote);

    /**
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Quote\Model\Quote $split
     * @return \Magestat\SplitOrder\Api\QuoteHandlerInterface
     */
    public function setCustomerData(Quote $quote,Quote $split);

    /**
     * Populate quotes with new data.
     *
     * @param array $quotes
     * @param \Magento\Quote\Model\Quote $split
     * @param \Magento\Quote\Model\Quote\Item[] $items
     * @param array $addresses
     * @param string $paymentMethod
     * @return \Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface
     */
    public function populateQuote($quotes,Quote $split, $items, $addresses, $paymentMethod);

    /**
     * Recollect order totals.
     *
     * @param array $quotes
     * @param \Magento\Quote\Model\Quote\Item[] $items
     * @param \Magento\Quote\Model\Quote $quote
     * @param array $addresses
     * @return \Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface
     */
    public function recollectTotal($quotes, $items,Quote $quote, $addresses);

    /**
     * @param array $quotes
     * @param \Magento\Quote\Model\Quote $quote
     * @param float $total
     */
    public function shippingAmount($quotes,Quote $quote, $total = 0.0);

    /**
     * Set payment method.
     *
     * @param string $paymentMethod
     * @param \Magento\Quote\Model\Quote $split
     * @param string $payment
     * @return \Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface
     */
    public function setPaymentMethod($paymentMethod, $split, $payment);

    /**
     * Define checkout sessions.
     *
     * @param \Magento\Quote\Model\Quote $split
     * @param \Magento\Framework\Model\AbstractExtensibleModel|\Magento\Sales\Api\Data\OrderInterface|object|null $order
     * @param array $orderIds
     * @return \Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface
     */
    public function defineSessions(Quote $split, $order, $orderIds);

    /**
     * Get quote address by customer address ID and store ID.
     *
     * @param int|string $addressId
     * @return Address|false
     */
    public function getShippingAddressByStoreCustomerAddressId($addressId,$storeId);

    /**
     * @return mixed
     */
    public function splitQuoteAdressesses();
}