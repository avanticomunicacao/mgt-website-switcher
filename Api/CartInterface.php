<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Api;

/**
 * Interface QuoteRepositoryInterface
 * @api
 */
interface CartInterface
{
    /**
     * @param string $code
     * @return mixed
     */
    public function updateCart(string $code);

    /**
     * @param string $code
     * @return mixed
     */
    public function getWebsiteId(string $code);

    /**
     * @param int $websiteId
     * @return mixed
     */
    public function getStoreIdByWebsiteId(int $websiteId);

    /**
     * @param int $productId
     * @param int $websiteId
     * @return mixed
     */
    public function getStockQty(int $productId,int $websiteId = null);

    /**
     * @param int $websiteId
     * @return mixed
     */
    public function getWebsiteCodeById(int $websiteId);
}