<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Api;

use Magento\Store\Model\Website;

/**
 * Interface InventoryInterface
 * @api
 */
interface InventoryInterface
{
    /**
     * @param int $stockId
     * @return array
     */
    public function getAssignedSource(int $stockId);

    /**
     * @param Website $website
     * @return mixed
     */
    public function getStockByWebsite(Website $website);

    /**
     * @param $sourceCode
     * @return mixed
     */
    public function getSourceAddress($sourceCode);
}