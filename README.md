# WEBSITE SWITCHER - M2 #

Magento 2 module: features of websites chages

### Instalation ###

* Composer

Add in 'repositories' of composer.json (magento 2 project):

     "repositories": {
        "mgt-website-switcher": {
            "url": "https://bitbucket.org/avanticomunicacao/mgt-website-switcher.git",
            "type": "git"
        }
     }

Make a require:

    composer require penseavanti/module-website-switcher:dev-master

### IMPORTANT ###

* Mode 1 (check website cookie name in admin painel and change if necessary)

Is necessary make the changes in index.php (pub folder) after $params = $_SERVER

    if (isset($_COOKIE['website_code'])) {
        $params[\Magento\Store\Model\StoreManager::PARAM_RUN_CODE] =  $_COOKIE['website_code'];
        $params[\Magento\Store\Model\StoreManager::PARAM_RUN_TYPE] = 'website';
    }

* Mode 2 

Point your HTTP server to index2.php in the project's pub folder and run the command cli 
    
    php bin/magento index:generate

### ADOBE CLOUD ###

Make this changes in .magento.app.yaml in the root folder

* web: locations: "/":

    passthru: "/index2.php"
    index: - index2.php
    
* hooks:post_deploy: |

    php bin/magento index:generate

### Relevant information ###
    
Same login for a customer work at multiple websites

    Stores --> Configuration --> Customers --> Customer Configuration --> Account Sharing Configuration:  GLOBAL
    
Price Scope

    Stores --> Configuration --> Catalog --> Catalog --> Price --> Catalog Price Scope: Website 
