/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

var config = {
    config: {
        mixins: {
            'mage/collapsible': {
                'Avanti_WebsiteSwitcher/js/mage/collapsible-mixin': true
            },
            'Magento_Swatches/js/swatch-renderer': {
                'Avanti_WebsiteSwitcher/js/mage/swatch-renderer-mixin': true
            },
            'Magento_Checkout/js/view/shipping': {
                'Avanti_WebsiteSwitcher/js/view/shipping-mixin': true
            }
        }
    },
    deps: [
        "Avanti_WebsiteSwitcher/js/switch-alert-modal"
    ],
    map: {
        '*': {
            'Magento_Checkout/template/summary/item/details':
                'Avanti_WebsiteSwitcher/template/summary/item/details',
            'Magento_Checkout/template/summary/item/details/thumbnail':
                'Avanti_WebsiteSwitcher/template/summary/item/details/thumbnail',
            'Magento_Checkout/template/summary/item/details/message':
                'Avanti_WebsiteSwitcher/template/summary/item/details/message',
            'Magento_Checkout/template/minicart/item/default':
                'Avanti_WebsiteSwitcher/template/minicart/item/default',
            'Amasty_Checkout/template/onepage/shipping/methods':
                'Avanti_WebsiteSwitcher/template/onepage/shipping/methods',
            'Magento_Checkout/js/model/shipping-save-processor/payload-extender':
                'Avanti_WebsiteSwitcher/js/model/shipping-save-processor/payload-extender',
            'Magento_Checkout/js/model/quote':
                'Avanti_WebsiteSwitcher/js/model/quote',
            'Amasty_Checkout/js/model/cart/totals-processor/default':
                'Avanti_WebsiteSwitcher/js/checkout/model/cart/totals-processor/default',
            'Amasty_Checkout/js/model/checkout-data-resolver-mixin':
                'Avanti_WebsiteSwitcher/js/model/checkout-data-resolver-mixin',
        }
    }
};
