/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

define([
    'jquery',
    'Magento_Checkout/js/view/summary/shipping',
    'Avanti_WebsiteSwitcher/js/model/quote'
], function ($, Component, quote) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Avanti_WebsiteSwitcher/checkout/summary/shipping'
        },

        quoteIsVirtual: quote.isVirtual(),
        quoteIsMultiShipping: quote.getIsMultiShipping(),
        allShippingAddresses: quote.getAllShippingAddresses(),

        getShippingMethodTitleByStore: function () {
            var shippingMethod,
                shippingMethodTitle = '',
                shippingStoreName = '';

            var allShippingAddresses = quote.getAllShippingAddresses();
            allShippingAddresses.forEach(this.setShippingMethodInAdress);

            if (!this.isCalculated()) {
                return '';
            }
            shippingMethod = quote.shippingMethod();

            if (!_.isArray(shippingMethod) && !_.isObject(shippingMethod)) {
                return '';
            }

            if (typeof shippingMethod['method_title'] !== 'undefined') {
                shippingMethodTitle = ' - ' + shippingMethod['method_title'];
            }

            if (typeof shippingMethod['extension_attributes'] !== 'undefined') {
                if (shippingMethod['extension_attributes']['store_name'] !== 'undefined') {
                    shippingStoreName = shippingMethod['extension_attributes']['store_name'] + ': ';
                }
            }

            var shippingMethods = '';

            for (var addressShipping of quote.getAllShippingAddresses() ) {
                shippingMethods += addressShipping.store_name + ': ';
                if (addressShipping.shipping_method !== null) {
                    shippingMethods += addressShipping.shipping_method.method_title ? addressShipping.shipping_method.carrier_title + ' - ' + addressShipping.shipping_method.method_title : addressShipping.shipping_method.carrier_title;
                }
                shippingMethods += '</br>';
            }

            return shippingMethods;
        },

        /**
         * @return {*|Boolean}
         */
        isCalculated: function () {
            return this.totals() && this.isFullMode() && quote.shippingMethod() != null; //eslint-disable-line eqeqeq
        },

        /**
         * @return {*}
         */
        getValue: function () {
            var price;

            if (!this.isCalculated()) {
                return this.notCalculatedMessage;
            }
            var quoteIsMultiShipping = quote.getIsMultiShipping();
            if (quoteIsMultiShipping) {
                var allShippingAddresses = quote.getAllShippingAddresses();
                if (this.partialShippingMethod()) {
                    return this.partialCalculatedMessage;
                }
                price = 0;
                for (var addressShipping of allShippingAddresses ) {
                    price += addressShipping.shipping_method.amount;
                }
                return this.getFormattedPrice(price);
            }

            price =  this.totals()['shipping_amount'];

            return this.getFormattedPrice(price);
        },

        /**
         * @return {*|Boolean}
         */
        partialShippingMethod : function () {
            var allShippingAddresses = quote.getAllShippingAddresses(),
                status = false;
            for (var addressShipping of allShippingAddresses ) {
                if (addressShipping['shipping_method'] == null) {
                    status =  true;
                }
            }
            return status;
        },

        setShippingMethodInAdress : function (addressShipping) {
            var shippingMethod = quote.shippingMethod(),
                shippingStoreId;

            if (shippingMethod == null) {
                return;
            }

            if (typeof shippingMethod['extension_attributes'] !== 'undefined') {
                if (shippingMethod['extension_attributes']['store_name'] !== 'undefined') {
                    shippingStoreId = shippingMethod['extension_attributes']['store_id'];
                }
            }

            if (shippingStoreId) {
                if (addressShipping.store_id === shippingStoreId) {
                    addressShipping['shipping_method'] = shippingMethod;
                }
            }
        }
    });
});
