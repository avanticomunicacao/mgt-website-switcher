/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

require([
    'jquery',
    'Magento_Ui/js/modal/confirm'
], function($, confirmation) {
    'use strict';
    if (window.showModal) {
        $('.switcher-option').click(function (event) {
            event.preventDefault();
            var url = event.target.href;
            confirmation({
                title: $.mage.__(window.titleModal),
                content: $.mage.__(window.avantiContentModal),
                actions: {
                    confirm: function(){
                        window.location.href = url;
                    },
                    cancel: function(){}
                },
                buttons: [{
                    text: $.mage.__(window.avantiDeclineButton),
                    class: 'action-secondary action-dismiss',
                    click: function (event) {
                        this.closeModal(event);
                    }
                }, {
                    text: $.mage.__(window.avantiAcceptedButton),
                    class: 'action primary action-accept',
                    click: function (event) {
                        this.closeModal(event, true);
                    }
                }]
            });
        });
    }
});