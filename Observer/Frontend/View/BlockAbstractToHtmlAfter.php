<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Observer\Frontend\View;

use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;

/**
 * Class BlockAbstractToHtmlBefore
 *
 * @package Avanti\WebsiteSwitcher\Observer\Frontend\View
 */
class BlockAbstractToHtmlAfter implements ObserverInterface
{

    /**
     * @var System
     */
    private $system;
    /**
     * @var Data
     */
    private $data;

    /**
     * BlockAbstractToHtmlAfter constructor.
     * @param System $system
     * @param Data $data
     */
    public function __construct(
        System $system,
        Data $data
    ) {
        $this->system = $system;
        $this->data = $data;
    }

    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(
        Observer $observer
    ) {
        if (!$this->system->isEnabled()) {
            return;
        }
        $template = $observer->getData('block')->getTemplate();
        if (
            $template === Config::TEMPLATE_CONFIGURABLE_PRODUCT_FINAL_PRICE
        ) {
            $html = $observer->getData('transport')->getData('html');
            $observer->getData('transport')->setData('html',$this->insertStoreIdHtml($html));
        }
    }

    /**
     * @param string $html
     * @return string
     */
    public function insertStoreIdHtml(string $html) :string
    {
        if (empty($html)) {
            return $html;
        }
        $htmlExplode = explode("\"",$html);
        array_splice($htmlExplode, 4, 0, ' data-product-store-id=');
        array_splice($htmlExplode, 5, 0, $this->data->getStoreId());
        return implode("\"",$htmlExplode);
    }
}