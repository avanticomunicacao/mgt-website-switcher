<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Observer\Frontend\View;

use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\View\Element\Context;

/**
 * Class BlockAbstractToHtmlBefore
 *
 * @package Avanti\WebsiteSwitcher\Observer\Frontend\View
 */
class BlockAbstractToHtmlBefore implements ObserverInterface
{
    /**
     * @var \Magento\Framework\App\CacheInterface
     */
    protected $_cache;

    private $messageManager;
    /**
     * @var Data
     */
    private $helperData;
    /**
     * @var System
     */
    private $system;

    /**
     * BlockAbstractToHtmlBefore constructor.
     * @param Context $context
     * @param ManagerInterface $messageManager
     * @param Data $helperData
     * @param System $system
     */
    public function __construct(
        Context $context,
        ManagerInterface $messageManager,
        Data $helperData,
        System $system
    )
    {
        $this->_cache = $context->getCache();
        $this->messageManager = $messageManager;
        $this->helperData = $helperData;
        $this->system = $system;
    }

    /**
     * Execute observer
     *
     * @param Observer $observer
     * @return void
     */
    public function execute(
        Observer $observer
    ) {
        if (!$this->system->isEnabled()) {
            return;
        }
        $template = $observer->getData('block')->getTemplate();
        if (
            $template === Config::TEMPLATE_PRODUCT_VIEW_OPTIONS_WRAPPER ||
            $template === Config::TEMPLATE_PRODUCT_PRICE_FINAL_PRICE ||
            $template === Config::TEMPLATE_PRODUCT_VIEW_FORMTABS ||
            $template === Config::TEMPLATE_PRODUCT_OPTIONS ||
            $template === Config::SWATCH_RENDERER_TEMPLATE ||
            $template === Config::MAGENTO_THEME_INDENTIFICATION_TEMPLATE ||
            $template === Config::MAGENTO_THEME_INDENTIFICATION_INFO_TEMPLATE
        ) {
            $block = $observer->getData('block');
            try {
                $this->_cache->remove($block->getCacheKey());
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong with cache.'));
            }
        }
    }
}
