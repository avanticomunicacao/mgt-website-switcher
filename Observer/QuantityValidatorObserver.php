<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Observer;

use Avanti\WebsiteSwitcher\Helper\Data;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\CatalogInventory\Model\Quote\Item\QuantityValidator;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class QuantityValidatorObserver implements ObserverInterface
{
    /**
     * @var QuantityValidator $quantityValidator
     */
    protected $quantityValidator;

    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var int
     */
    private $storeId;

    /**
     * @param QuantityValidator $quantityValidator
     * @param ProductRepositoryInterface $_productRepository
     * @param Data $data
     */
    public function __construct(
        QuantityValidator $quantityValidator,
        ProductRepositoryInterface $_productRepository,
        Data $data
    ) {
        $this->quantityValidator = $quantityValidator;
        $this->_productRepository = $_productRepository;
        $this->data = $data;
        $this->storeId = $data->getStoreId();
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $quoteItem = $observer->getEvent()->getItem();
        $this->data->changeStore($quoteItem->getStoreId());
        if (!$quoteItem ||
            !$quoteItem->getProductId() ||
            !$quoteItem->getQuote()
        ) {
            return;
        }
        $qty = $quoteItem->getQty();
        $options = $quoteItem->getQtyOptions();
        if (($options = $quoteItem->getQtyOptions()) && $qty > 0) {
            foreach ($options as $option) {
                $option->setProduct($this->getProduct($option->getProduct()));
            }
        }
        $this->quantityValidator->validate($observer);
        $this->data->changeStore($this->storeId);
    }

    /**
     * @param Product $item
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws NoSuchEntityException
     */
    public function getProduct(Product $item)
    {
        switch ($item->getTypeId()) {
            case Configurable::TYPE_CODE :
                $product =  $this->_productRepository->get(
                    $this->_productRepository->getById($item->getEntityId())->getSku(),
                    false,
                    $this->data->getStoreId(),
                    true);
                try {
                    $this->updatePrice($item);
                } catch (NoSuchEntityException $e) {
                }
                break;
            default :
                $product =  $this->_productRepository->get(
                    $item->getSku(),
                    false,
                    $this->data->getStoreId(),
                    true);
        }
        return $product;
    }

    /**
     * @param $item
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updatePrice($item)
    {
        if (!empty($item->getProduct())) {
            $simpleProductOption = $item->getProduct()->getCustomOption('simple_product');
            if (!empty($simpleProductOption)) {
                $simpleProduct = $simpleProductOption->getProduct();
                if (!empty($simpleProduct)) {
                    $storeProduct =  $this->_productRepository->get(
                        $this->_productRepository->getById($simpleProduct->getId())->getSku(),
                        false,
                        $item->getStoreId(),
                        true);
                    $simpleProductOption->setProduct($storeProduct);
                    $item->setCustomOption('simple_product',$simpleProductOption);
                }
            }
        }
    }
}