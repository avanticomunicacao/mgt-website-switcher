<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Observer;

use Magento\Quote\Model\Quote\Item as QuoteItem;

/**
 * Prepare array with information about used product id and product store id
 */
class ProductStore
{
    /**
     * Prepare array with information about used product id and product stock item
     *
     * @param array $relatedItems
     * @return array
     */
    public function getProductStore($relatedItems)
    {
        $items = [];
        foreach ($relatedItems as $item) {
            $productId = $item->getProductId();
            if (!$productId) {
                continue;
            }
            $children = $item->getChildrenItems();
            if ($children) {
                foreach ($children as $childItem) {
                    $this->_addItemToStoreArray($childItem, $items);
                }
            } else {
                $this->_addItemToStoreArray($item, $items);
            }
        }
        return $items;
    }

    /**
     * Adds stock item store to $items (creates new entry or override existing one)
     *
     * @param QuoteItem $quoteItem
     * @param array &$items
     * @return void
     */
    protected function _addItemToStoreArray(QuoteItem $quoteItem, &$items)
    {
        $productId = $quoteItem->getProductId();
        if (!$productId) {
            return;
        }
        if (isset($items[$productId])) {
            $items[$productId] = $quoteItem->getStoreId();
        } else {
            $items[$productId] = $quoteItem->getStoreId();
        }
    }
}