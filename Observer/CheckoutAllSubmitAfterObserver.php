<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Observer;

use Magento\CatalogInventory\Observer\ReindexQuoteInventoryObserver;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer as EventObserver;

/**
 * Catalog inventory module Checkout observer
 */
class CheckoutAllSubmitAfterObserver implements ObserverInterface
{
    /**
     * @var SubtractQuoteInventoryObserver
     */
    protected $subtractQuoteInventoryObserver;

    /**
     * @var ReindexQuoteInventoryObserver
     */
    protected $reindexQuoteInventoryObserver;

    /**
     * @param SubtractQuoteInventoryObserver $subtractQuoteInventoryObserver
     * @param ReindexQuoteInventoryObserver $reindexQuoteInventoryObserver
     */
    public function __construct(
        SubtractQuoteInventoryObserver $subtractQuoteInventoryObserver,
        ReindexQuoteInventoryObserver $reindexQuoteInventoryObserver
    ) {
        $this->subtractQuoteInventoryObserver = $subtractQuoteInventoryObserver;
        $this->reindexQuoteInventoryObserver = $reindexQuoteInventoryObserver;
    }

    /**
     * Subtract qtys of quote item products after multishipping checkout
     *
     * @param EventObserver $observer
     * @return \Magento\CatalogInventory\Observer\CheckoutAllSubmitAfterObserver
     */
    public function execute(EventObserver $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        if (!$quote->getInventoryProcessed()) {
            $this->subtractQuoteInventoryObserver->execute($observer);
            $this->reindexQuoteInventoryObserver->execute($observer);
        }
        return $this;
    }
}