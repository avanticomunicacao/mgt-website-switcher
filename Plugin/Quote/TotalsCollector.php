<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;
use Magento\Checkout\Model\Session;

/**
 * Class TotalsCollector
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Magento\Quote\Model\Quote
 */
class TotalsCollector
{
    /**
     * @var System
     */
    private $system;

    /**
     * @var ProductFactory
     */
    private $_productFactory;

    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepository;

    /**
     * QuoteCollectTotalsBefore constructor.
     * @param System $system
     * @param ProductFactory $_productFactory
     * @param ProductRepositoryInterface $_productRepository
     */
    public function __construct(
        System $system,
        ProductFactory $_productFactory,
        Session $checkoutSession,
        ProductRepositoryInterface $_productRepository
    ) {
        $this->system = $system;
        $this->_productFactory = $_productFactory;
        $this->_productRepository = $_productRepository;
        $this->_checkoutSession = $checkoutSession;
    }

    /**
     * @param \Magento\Quote\Model\Quote\TotalsCollector $subject
     * @param $quote
     * @return array
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function beforeCollect(
        \Magento\Quote\Model\Quote\TotalsCollector $subject,
        $quote
    ) {
        if ($this->system->isEnabled() && $this->system->shareQuote() && $this->checkItems($quote)) {
            foreach ($quote->getItems() as $item) {
                $item->setProduct($this->getProduct($item));
                $items[] = $item;
            }
            if (isset($items)) {
                $quote->setItems($items);
            }
            if ($quote->getCouponCode()) {
                $this->_checkoutSession->setSkipValidate(true);
            }
        }
        return [$quote];
    }

    /**
     * @param $quote
     * @return bool
     */
    public function checkItems($quote)
    {
        return ($quote->hasItems() && !empty($quote->getItems()));
    }

    /**
     * @param $item
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProduct($item)
    {
        switch ($item->getProductType()) {
            case Configurable::TYPE_CODE :
                $product =  $this->_productRepository->get(
                    $this->_productRepository->getById($item->getProductId())->getSku(),
                    false,
                    $item->getStoreId(),
                    true);
                $this->updatePrice($item);
                break;
            default :
                $product =  $this->_productRepository->get(
                    $item->getSku(),
                    false,
                    $item->getStoreId(),
                    true);
        }
        return $product;
    }

    /**
     * @param $item
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function updatePrice($item)
    {
        if (!empty($item->getProduct())) {
            $simpleProductOption = $item->getProduct()->getCustomOption('simple_product');
            if (!empty($simpleProductOption)) {
                $simpleProduct = $simpleProductOption->getProduct();
                if (!empty($simpleProduct)) {
                    $storeProduct =  $this->_productRepository->get(
                        $this->_productRepository->getById($simpleProduct->getId())->getSku(),
                        false,
                        $item->getStoreId(),
                        true);
                    $simpleProductOption->setProduct($storeProduct);
                    $item->setCustomOption('simple_product',$simpleProductOption);
                }
            }
        }
    }
}