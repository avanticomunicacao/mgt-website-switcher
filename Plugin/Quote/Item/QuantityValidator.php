<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Item;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Event\Observer;

/**
 * Class QuantityValidator
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Magento\CatalogInventory\Model\Quote\Item\QuantityValidator
 */
class QuantityValidator
{
    /**
     * @var System
     */
    private $system;

    /**
     * @var ProductFactory
     */
    private $_productFactory;

    /**
     * StockItem constructor.
     * @param System $system
     * @param ProductFactory $_productFactory
     */
    public function __construct(
        System $system,
        ProductFactory $_productFactory

    ) {
        $this->system = $system;
        $this->_productFactory = $_productFactory;
    }

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator $subject
     * @param Observer $observer
     * @return Observer[]
     */
    public function beforeValidate(
        \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator $subject,
        Observer $observer
    ) {
        $quoteItem = $observer->getEvent()->getItem();
        if ($this->system->isEnabled() && $this->system->shareQuote() && !is_null($quoteItem->getItemId())) {
            $product = $this->_productFactory->create()->setStoreId($quoteItem->getStoreId())->load($quoteItem->getData('product_id'));
            $quoteItem->setProduct($product);
            $observer->getEvent()->setItem($quoteItem);
        }
        return [$observer];
    }
}