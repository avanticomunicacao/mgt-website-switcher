<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Item\QuantityValidator\Initializer;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Model\ProductFactory;

/**
 * Class StockItem
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer
 */
class StockItem
{

    /**
     * @var System
     */
    private $system;
    /**
     * @var ProductFactory
     */
    private $_productFactory;

    /**
     * StockItem constructor.
     * @param System $system
     * @param ProductFactory $_productFactory
     */
    public function __construct(
        System $system,
        ProductFactory $_productFactory

    ) {
        $this->system = $system;
        $this->_productFactory = $_productFactory;
    }

    /**
     * @param \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem $subject
     * @param $stockItem
     * @param $quoteItem
     * @param $qty
     * @return array
     */
    public function beforeInitialize(
        \Magento\CatalogInventory\Model\Quote\Item\QuantityValidator\Initializer\StockItem $subject,
        $stockItem,
        $quoteItem,
        $qty
    ) {
        if ($this->system->isEnabled() && $this->system->shareQuote() && !is_null($quoteItem->getItemId())) {
            $product = $this->_productFactory->create()->setStoreId($quoteItem->getStoreId())->load($quoteItem->getData('product_id'));
            $quoteItem->setProduct($product);
        }
        return [$stockItem,$quoteItem,$qty];
    }
}