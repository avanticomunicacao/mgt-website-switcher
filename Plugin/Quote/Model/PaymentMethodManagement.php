<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Model;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Customer\Api\AddressRepositoryInterface;
use Magento\Framework\Exception\State\InvalidTransitionException;
use Magento\Payment\Model\Checks\ZeroTotal;
use Magento\Payment\Model\Method\AbstractMethod;
use Magento\Quote\Api\CartRepositoryInterface;

/**
 * Class ShippingMethodManagement
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Quote\Model\PaymentMethodManagement
 */
class PaymentMethodManagement
{

    const FIRSTNAME = 'firstname';
    const LASTNAME = 'lastname';
    const STREET = 'street';
    const CITY = 'city';
    const TELEPHONE = 'telephone';

    protected $fields = [
        self::FIRSTNAME,
        self::LASTNAME,
        self::STREET,
        self::CITY,
        self::TELEPHONE
        ];

    /**
     * @var System
     */
    private $system;

    /**
     * @var ZeroTotal
     */
    private $zeroTotalValidator;

    /**
     * @var CartRepositoryInterface
     */
    private $_quoteRepository;

    /**
     * @var AddressRepositoryInterface
     */
    private $_addressRepository;

    /**
     * PaymentMethodManagement constructor.
     * @param System $system
     * @param ZeroTotal $zeroTotalValidator
     * @param CartRepositoryInterface $_quoteRepository
     * @param AddressRepositoryInterface $addressRepository
     */
    public function __construct(
        System $system,
        ZeroTotal $zeroTotalValidator,
        CartRepositoryInterface $_quoteRepository,
        AddressRepositoryInterface $addressRepository
    ) {
        $this->system = $system;
        $this->zeroTotalValidator = $zeroTotalValidator;
        $this->_quoteRepository = $_quoteRepository;
        $this->_addressRepository = $addressRepository;
    }

    public function aroundSet(
        \Magento\Quote\Model\PaymentMethodManagement $subject,
        \Closure $proceed,
        $cartId,
        $method
    ) {
        if (!$this->system->isEnabled()) {
            return $proceed($cartId, $method);
        }
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->_quoteRepository->get($cartId);

        $quote->setTotalsCollectedFlag(false);
        $method->setChecks([
            AbstractMethod::CHECK_USE_CHECKOUT,
            AbstractMethod::CHECK_USE_FOR_COUNTRY,
            AbstractMethod::CHECK_USE_FOR_CURRENCY,
            AbstractMethod::CHECK_ORDER_TOTAL_MIN_MAX,
        ]);

        if ($quote->isVirtual()) {
            $address = $quote->getBillingAddress();
        } else {
            $address = $quote->getShippingAddress();
            // check if shipping address is set
            if ($address->getCountryId() === null) {
                throw new InvalidTransitionException(
                    __('The shipping address is missing. Set the address and try again.')
                );
            }
            $address->setCollectShippingRates(true);
        }

        $paymentData = $method->getData();
        $payment = $quote->getPayment();
        $payment->importData($paymentData);
        $address->setPaymentMethod($payment->getMethod());
        $this->validateAddress($address);
        if (!$this->zeroTotalValidator->isApplicable($payment->getMethodInstance(), $quote)) {
            throw new InvalidTransitionException(__('The requested Payment Method is not available.'));
        }

        $quote->save();
        return $quote->getPayment()->getId();
    }

    public function validateAddress($address)
    {
        $customerAdress = $address->getQuote()->getCustomer()->getAddresses()[0];
        foreach ($this->fields as $field) {
            if (is_null($address->getData($field)) || !array_key_exists($field,$address->getData()) || empty($address->getData($field))) {
                switch ($field):
                    case self::FIRSTNAME:
                        $address->setData($field,$customerAdress->getFirstName());
                        break;
                    case self::LASTNAME:
                        $address->setData($field,$customerAdress->getLastName());
                        break;
                    case self::CITY:
                        $address->setData($field,$customerAdress->getCity());
                        break;
                    case self::STREET:
                        $address->setData($field,$this->getStreetFull($customerAdress));
                        break;
                    case self::TELEPHONE:
                        $address->setData($field, $customerAdress->getTelephone());
                        break;
                endswitch;
            }
        }
    }

    /**
     * Retrieve text of street lines, concatenated using LF symbol
     *
     * @return string
     */
    public function getStreetFull($customerAdress)
    {
        $street = $customerAdress->getStreet();
        return is_array($street) ? implode("\n", $street) : $street;
    }
}
