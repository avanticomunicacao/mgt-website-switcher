<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Model\Cart;

use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Exception\NoSuchEntityException;

/**
 * Class ShippingMethodConverter
 */
class ShippingMethodConverter
{

    /**
     * @var System
     */
    private $system;

    /**
     * @var Data
     */
    private $data;

    /**
     * ShippingMethodConverter constructor.
     * @param System $system
     * @param Data $data
     */
    public function __construct(
        System $system,
        Data $data
    ) {
        $this->system = $system;
        $this->data = $data;
    }

    /**
     * @param \Magento\Quote\Model\Cart\ShippingMethodConverter $subject
     * @param $result
     * @param $quoteCurrencyCode
     * @param $rateModel
     * @return mixed
     */
    public function afterModelToDataObject(
        \Magento\Quote\Model\Cart\ShippingMethodConverter $subject,
        $result,
        $quoteCurrencyCode,
        $rateModel
    ) {
        if (!$this->system->isEnabled()) {
            return $result;
        }
        try {
            $extensionAttributes = $result->getExtensionAttributes();
            $extensionAttributes->setStoreId($this->data->getStoreId());
            $extensionAttributes->setStoreName($this->data->getStoreById($this->data->getStoreId())->getName());
            $result->setExtensionAttributes($extensionAttributes);
        } catch (NoSuchEntityException $e) {
            return $result;
        }
        return $result;
    }
}