<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Model;

use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\System;
use Avanti\WebsiteSwitcher\Helper\Data;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Webapi\ServiceOutputProcessor;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\EstimateAddressInterface;
use Magento\Quote\Model\Cart\ShippingMethodConverter;
use Magento\Quote\Model\Quote;
use Magento\Quote\Model\Quote\TotalsCollector;
use \Avanti\WebsiteSwitcher\Helper\Config;
use Magento\Store\Model\Information;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Quote\Api\Data\AddressExtensionInterfaceFactory;
use Magento\Quote\Api\Data\AddressExtensionInterface;

/**
 * Class ShippingMethodManagement
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Quote\Model\ShippingMethodManagement
 */
class ShippingMethodManagement
{

    /**
     * @var int
     */
    private $storeId;

    /**
     * @var System
     */
    private $system;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var CartRepositoryInterface
     */
    private $_quoteRepository;

    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;

    /**
     * @var TotalsCollector
     */
    private $_totalsCollector;

    /**
     * @var ShippingMethodConverter
     */
    private $_converter;

    /**
     * @var \Magento\Framework\Reflection\DataObjectProcessor $dataProcessor
     */
    private $dataProcessor;

    /**
     * @var ServiceOutputProcessor
     */
    private $_serviceOutputProcessor;
    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Information
     */
    private $_storeInfo;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var AddressExtensionInterfaceFactory
     */
    private $addressExtensionInterfaceFactory;

    /**
     * ShippingMethodManagement constructor.
     * @param System $system
     * @param Data $data
     * @param CartRepositoryInterface $_quoteRepository
     * @param QuoteHandlerInterface $quoteHandler
     * @param TotalsCollector $totalsCollector
     * @param ShippingMethodConverter $_converter
     * @param ServiceOutputProcessor $_serviceOutputProcessor
     */
    public function __construct(
        System $system,
        Data $data,
        CartRepositoryInterface $_quoteRepository,
        QuoteHandlerInterface $quoteHandler,
        TotalsCollector $totalsCollector,
        ShippingMethodConverter $_converter,
        ServiceOutputProcessor $_serviceOutputProcessor,
        CheckoutSession $checkoutSession,
        Information $_storeInfo,
        StoreManagerInterface $storeManager,
        AddressExtensionInterfaceFactory $addressExtensionInterfaceFactory
    ) {
        $this->system = $system;
        $this->data = $data;
        $this->_quoteRepository = $_quoteRepository;
        $this->storeId = $data->getStoreId();
        $this->quoteHandler = $quoteHandler;
        $this->_totalsCollector = $totalsCollector;
        $this->_converter = $_converter;
        $this->_serviceOutputProcessor = $_serviceOutputProcessor;
        $this->checkoutSession = $checkoutSession;
        $this->_storeInfo = $_storeInfo;
        $this->_storeManager = $storeManager;
        $this->addressExtensionInterfaceFactory = $addressExtensionInterfaceFactory;
    }

    /**
     * @param \Magento\Quote\Model\ShippingMethodManagement $subject
     * @param $result
     * @param $addressId
     * @param $cartId
     * @return mixed
     */
    public function afterEstimateByAddressId(
        \Magento\Quote\Model\ShippingMethodManagement $subject,
        $result,
        $cartId,
        $addressId
    ) {
        if (!$this->system->isEnabled()) {
            return $result;
        }
        try {
            $estimated = [];
            $quote = $this->checkoutSession->getQuote();
            if ($this->quoteHandler->splitQuotes($quote)) {
                $this->quoteHandler->splitQuoteAdressesses();
            }
            foreach ($quote->getAllShippingAddresses() as $shippingAddresses) {
                if (!($shippingAddresses->getData('store_id'))) {
                    $shippingAddresses->setData('store_id',$this->getStoreIdByAddress($shippingAddresses));
                    $shippingAddresses->setData('store_info',$this->_storeInfo->getStoreInformationObject($this->_storeManager->getStore($shippingAddresses->getData('store_id'))));
                }
                $this->data->changeStore($shippingAddresses->getData('store_id'));
                $rates = $this->getShippingMethods($quote,$shippingAddresses);
                if (empty($rates)) {
                    foreach ($result as $shipping) {
                        $extensionAttribute = $shipping->getExtensionAttributes();
                        $extensionAttribute->setData('store_id',$shippingAddresses->getData('store_id'));
                        $extensionAttribute->setData('store_name',$this->_storeManager->getStore()->getName());
                        $shipping->setExtensionAttributes($extensionAttribute);
                    }
                    $rates = $result;
                }
                $estimated[] = array(
                    'store_name' => $shippingAddresses->getData('store_info')->getName(),
                    'store_id' => $shippingAddresses->getData('store_id'),
                    'rates' => $this->_serviceOutputProcessor->convertValue($rates,Config::MAGE_QUOTE_API_DATA_SHIPPING_METHOD_INTERFACE)
                );
            }
        } catch (NoSuchEntityException $e) {
            return $e;
        }
        $this->data->changeStore($this->storeId);
        return $estimated;
    }

    /**
     * @inheritdoc
     */
    public function afterEstimateByExtendedAddress(
        \Magento\Quote\Model\ShippingMethodManagement $subject,
        $result,
        $cartId,
        AddressInterface $address
    ) {
        if (!$this->system->isEnabled()) {
            return $result;
        }
        try {
            $estimated = [];
            $quote = $this->checkoutSession->getQuote();
            if ($this->quoteHandler->splitQuotes($quote)) {
                $this->quoteHandler->splitQuoteAdressesses();
            }
            if ($quote->isVirtual() || 0 == $quote->getItemsCount()) {
                return [];
            }
            foreach ($quote->getAllShippingAddresses() as $shippingAddresses) {
                if (!($shippingAddresses->getData('store_id'))) {
                    $shippingAddresses->setData('store_id',$this->getStoreIdByAddress($shippingAddresses));
                    $shippingAddresses->setData('store_info',$this->_storeInfo->getStoreInformationObject($this->_storeManager->getStore($shippingAddresses->getData('store_id'))));
                }
                $this->data->changeStore($shippingAddresses->getData('store_id'));
                $rates = $this->getShippingMethods($quote,$shippingAddresses);
                if (empty($rates)) {
                    foreach ($result as $shipping) {
                        $extensionAttribute = $shipping->getExtensionAttributes();
                        $extensionAttribute->setData('store_id',$shippingAddresses->getData('store_id'));
                        $extensionAttribute->setData('store_name',$this->_storeManager->getStore()->getName());
                        $shipping->setExtensionAttributes($extensionAttribute);
                    }
                    $rates = $result;
                }
                $estimated[] = array(
                    'store_name' => $shippingAddresses->getData('store_info')->getName(),
                    'store_id' => $shippingAddresses->getData('store_id'),
                    'rates' => $this->_serviceOutputProcessor->convertValue($rates,Config::MAGE_QUOTE_API_DATA_SHIPPING_METHOD_INTERFACE)
                );
            }
        } catch (NoSuchEntityException $e) {
            return $e;
        }
        $this->data->changeStore($this->storeId);
        return $estimated;
    }

    /**
     * Get list of available shipping methods
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @param \Magento\Framework\Api\ExtensibleDataInterface $address
     * @return \Magento\Quote\Api\Data\ShippingMethodInterface[]
     */
    private function getShippingMethods(Quote $quote, $address)
    {
        $output = [];
        $shippingAddress = $quote->getShippingAddress();
        $shippingAddress->addData($this->extractAddressData($address));
        $shippingAddress->setCollectShippingRates(true);
        $shippingAddressExtensionAttributes = $this->extractAddressExtensionAttributes($address->getExtensionAttributes());
        $shippingAddress->setExtensionAttributes($shippingAddressExtensionAttributes);
        $this->_totalsCollector->collectAddressTotals($quote, $shippingAddress);
        $shippingRates = $shippingAddress->getGroupedAllShippingRates();
        foreach ($shippingRates as $carrierRates) {
            foreach ($carrierRates as $rate) {
                $output[] = $this->_converter->modelToDataObject($rate, $quote->getQuoteCurrencyCode());
            }
        }
        return $output;
    }

    /**
     * Get transform address interface into Array
     *
     * @param \Magento\Framework\Api\ExtensibleDataInterface  $address
     * @return array
     */
    private function extractAddressData($address)
    {
        $className = \Magento\Customer\Api\Data\AddressInterface::class;
        if ($address instanceof \Magento\Quote\Api\Data\AddressInterface) {
            $className = \Magento\Quote\Api\Data\AddressInterface::class;
        } elseif ($address instanceof EstimateAddressInterface) {
            $className = EstimateAddressInterface::class;
        }
        return $this->getDataObjectProcessor()->buildOutputDataArray(
            $address,
            $className
        );
    }

    /**
     * Gets the data object processor
     *
     * @return \Magento\Framework\Reflection\DataObjectProcessor
     * @deprecated 101.0.0
     */
    private function getDataObjectProcessor()
    {
        if ($this->dataProcessor === null) {
            $this->dataProcessor = ObjectManager::getInstance()
                ->get(DataObjectProcessor::class);
        }
        return $this->dataProcessor;
    }

    public function getStoreIdByAddress($address)
    {
        foreach ($address->getAllVisibleItems() as $item) {
            if ($item->getStoreId()) {
                return $item->getStoreId();
            }
        }
        return false;
    }

    private function extractAddressExtensionAttributes($addressExtensionAttributes)
    {
        if ($addressExtensionAttributes instanceof AddressExtensionInterface) {
            return $addressExtensionAttributes;
        } else {
            return $this->addressExtensionInterfaceFactory->create();
        }
    }
}
