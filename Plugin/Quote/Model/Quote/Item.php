<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Model\Quote;

use Avanti\WebsiteSwitcher\Helper\System;

/**
 * Class Item
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Quote\Model\Quote\Item
 */
class Item
{
    /**
     * @var System
     */
    protected $system;

    public function __construct(
        System $system
    ) {
        $this->system = $system;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $subject
     * @param \Closure $proceed
     * @param $product
     * @return mixed
     */
    public function aroundSetProduct(
        \Magento\Quote\Model\Quote\Item $subject,
        \Closure $proceed,
        $product
    ) {
        if ($subject->getQuote() && $this->isEnabled()) {
            $originStoreId = (int)$product->getStoreId();
        }
        $result = $proceed($product);
        if ($subject->getQuote() && $this->isEnabled()) {
            $result->getData('product')->setStoreId($originStoreId);
        }
        return $result;
    }

    /**
     * @param \Magento\Quote\Model\Quote\Item $subject
     * @param \Closure $proceed
     * @param $quote
     * @return mixed
     */
    public function aroundSetQuote(
        \Magento\Quote\Model\Quote\Item $subject,
        \Closure $proceed,
        $quote
    ) {
        if ($this->isEnabled()) {
            if (!is_null($subject->getStoreId())) {
                $originStoreId = (int)$subject->getStoreId();
            }
        }
        $result = $proceed($quote);
        if ($this->isEnabled()) {
            if (!is_null($result->getStoreId())) {
                $result->setStoreId($originStoreId);
            }
        }
        return $result;
    }

    /**
     * @return bool|mixed
     */
    public function isEnabled()
    {
        return ($this->system->isEnabled() && $this->system->shareQuote());
    }
}