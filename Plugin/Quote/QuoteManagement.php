<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote;

use Magento\Framework\Exception\LocalizedException;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Model\QuoteFactory;
use Magento\Framework\Event\ManagerInterface;
use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;

/**
 * Class QuoteManagement
 * Interceptor to \Magento\Quote\Model\QuoteManagement
 */
class QuoteManagement
{
    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var QuoteFactory
     */
    private $quoteFactory;

    /**
     * @var ManagerInterface
     */
    private $eventManager;

    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var System
     */
    private $system;

    /**
     * @param CartRepositoryInterface $quoteRepository
     * @param QuoteFactory $quoteFactory
     * @param ManagerInterface $eventManager
     * @param QuoteHandlerInterface $quoteHandler
     * @param Data $data
     * @param System $system
     */
    public function __construct(
        CartRepositoryInterface $quoteRepository,
        QuoteFactory $quoteFactory,
        ManagerInterface $eventManager,
        QuoteHandlerInterface $quoteHandler,
        Data $data,
        System $system
    ) {
        $this->quoteRepository = $quoteRepository;
        $this->quoteFactory = $quoteFactory;
        $this->eventManager = $eventManager;
        $this->quoteHandler = $quoteHandler;
        $this->data = $data;
        $this->system = $system;
    }

    /**
     * @param \Magento\Quote\Model\QuoteManagement $subject
     * @param \Closure $proceed
     * @param $cartId
     * @param null $paymentMethod
     * @return array|mixed
     * @throws LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundPlaceOrder(
        \Magento\Quote\Model\QuoteManagement $subject,
        \Closure $proceed,
        $cartId,
        $paymentMethod = null
    ) {
        $currentQuote = $this->quoteRepository->getActive($cartId);
        if ((!$this->system->isEnabled() &&
            !$this->system->isSplitOrder() &&
            !$this->system->shareQuote())
        ) {
            $result = $proceed($cartId,$paymentMethod);
            return $result;
        }

        // Separate all items in quote into new quotes.
        $quotes = $this->quoteHandler->splitQuotes($currentQuote);
        if (empty($quotes)) {
            return $result = array_values([($proceed($cartId, $paymentMethod))]);
        }
        // Collect list of data addresses.
        $addresses = $this->quoteHandler->collectAddressesData($currentQuote);

        /** @var \Magento\Sales\Api\Data\OrderInterface[] $orders */
        $orders = [];
        $orderIds = [];
        foreach ($quotes as $key => $items) {
            /** @var \Magento\Quote\Model\Quote $split */
            $split = $this->quoteFactory->create();
            // Set all customer definition data.
            $this->quoteHandler->setCustomerData($currentQuote, $split);

            // Map quote items.
            foreach ($items as $item) {
                // Add item by item.
                $item->setId(null);
                $split->addItem($item);
            }
            $split->setStoreId($key);
            $this->quoteHandler->populateQuote($quotes, $split, $items, $addresses, $paymentMethod);
            // Dispatch event as Magento standard once per each quote split.
            $this->eventManager->dispatch(
                'checkout_submit_before',
                ['quote' => $split]
            );
            $split->setStoreId($key);
            $split->setData('totals_collected_flag',true);
            $shippingAddress = $addresses['shipping'];
            if ($addresses['is_multi_shipping']) {
                foreach ($shippingAddress as $shipping) {
                    if ($split->getStoreId() === $shipping['store_id']) {
                        $shippingAddress = $shipping;
                        break;
                    }
                }
            }
            $split->setAppliedRuleIds($shippingAddress['applied_rule_ids']);
            $this->toSaveQuote($split);
            $order = $subject->submit($split);
            if (is_null($order)) {
                throw new LocalizedException(__('Please try to place the order again.'));
            }
            $orders[] = $order;
            $orderIds[$order->getId()] = $order->getIncrementId();
        }
        $currentQuote->setIsActive(false);
        $this->toSaveQuote($currentQuote);

        $this->quoteHandler->defineSessions($split, $order, $orderIds);

        $this->eventManager->dispatch(
            'checkout_submit_all_after',
            ['orders' => $orders, 'quote' => $currentQuote]
        );
        return $this->getOrderKeys($orderIds);
    }

    /**
     * Save quote
     *
     * @param \Magento\Quote\Api\Data\CartInterface $quote
     * @return \Magestat\SplitOrder\Plugin\SplitQuote
     */
    private function toSaveQuote($quote)
    {
        $this->quoteRepository->save($quote);

        return $this;
    }

    /**
     * @param array $orderIds
     * @return array
     */
    private function getOrderKeys($orderIds)
    {
        $orderValues = [];
        foreach (array_keys($orderIds) as $orderKey) {
            $orderValues[] = (string) $orderKey;
        }
        return array_values($orderValues);
    }

    private function isMultiShipping($quote)
    {
        if (!is_null($quote->getIsMultiShipping()))
        {
            return $quote->getIsMultiShipping();
        }
        return false;
    }
}
