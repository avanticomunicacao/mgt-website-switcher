<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote;

/**
 * Class Item
 */
class Item
{
    /**
     * @param \Magento\Quote\Model\Quote\Item $subject
     * @param $result
     * @param $product
     * @return bool
     */
    public function afterRepresentProduct(
        \Magento\Quote\Model\Quote\Item $subject,
        $result,
        $product
    ) {
        $itemProduct = $subject->getProduct();
        if ((int)$itemProduct->getStoreId() != (int)$product->getStoreId()) {
            return false;
        }
        return $result;
    }
}