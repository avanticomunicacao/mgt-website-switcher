<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Api;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\ExtensibleDataInterface;
use Magento\Quote\Api;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CouponManagementInterface;
use Magento\Quote\Model\Cart\CartTotalRepository;
use Magento\Quote\Model\Cart\Totals\ItemConverter;
use Magento\Quote\Model\Cart\TotalsConverter;
use Magento\Quote\Model\Quote;

class CartTotalRepositoryInterface extends CartTotalRepository
{

    private $total = [
        'weight',
        'subtotal',
        'base_subtotal',
        'subtotal_with_discount',
        'base_subtotal_with_discount',
        'tax_amount',
        'base_tax_amount',
        'shipping_amount',
        'base_shipping_amount',
        'shipping_tax_amount',
        'base_shipping_tax_amount',
        'discount_amount',
        'base_discount_amount',
        'grand_total',
        'base_grand_total',
        'shipping_discount_amount',
        'base_shipping_discount_amount',
        'subtotal_incl_tax',
        'base_subtotal_total_incl_tax',
        'discount_tax_compensation_amount',
        'base_discount_tax_compensation_amount',
        'shipping_discount_tax_compensation_amount',
        'base_shipping_discount_tax_compensation_amnt',
        'shipping_incl_tax',
        'base_shipping_incl_tax',
        'total_qty',
        'item_qty',
        'base_subtotal_incl_tax',
        'weee_amount',
        'base_weee_amount',
        'shipping_tax_calculation_amount',
        'base_shipping_tax_calculation_amount',
        'base_shipping_discount_tax_compensation_amount',
        'amasty_checkout_amount',
        'base_amasty_checkout_amount',
        'shipping_amount_for_discount',
        'base_shipping_amount_for_discount',
        'extra_tax_amount',
        'base_extra_tax_amount',
        'extra_taxable_details',
        'weee_tax_amount',
        'base_weee_tax_amount'
    ];

    private $exclude_totals = [
        'code',
        'address',
        'title',
        'area',
        'full_info'
    ];

    /**
     * @var CartRepositoryInterface
     */
    private $quoteRepository;

    /**
     * @var System
     */
    private $system;

    /**
     * @var Api\Data\TotalsInterfaceFactory
     */
    private $totalsFactory;

    /**
     * @var DataObjectHelper
     */
    private $dataObjectHelper;

    /**
     * @var itemConverter
     */
    private $itemConverter;

    public function __construct(
        Api\Data\TotalsInterfaceFactory $totalsFactory,
        CartRepositoryInterface $quoteRepository,
        DataObjectHelper $dataObjectHelper,
        CouponManagementInterface $couponService,
        TotalsConverter $totalsConverter,
        ItemConverter $converter,
        System $system
    ) {
        parent::__construct(
            $totalsFactory,
            $quoteRepository,
            $dataObjectHelper,
            $couponService,
            $totalsConverter,
            $converter
        );
        $this->quoteRepository = $quoteRepository;
        $this->system = $system;
        $this->totalsFactory = $totalsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->itemConverter = $converter;
    }

    /**
     * @param Api\CartTotalRepositoryInterface $subject
     * @param \Closure $proceed
     * @param $cartId
     * @return Api\Data\TotalsInterface|mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundGet(
        Api\CartTotalRepositoryInterface $subject,
        \Closure $proceed,
        $cartId
    ) {
        /** @var Quote $quote */
        $quote = $this->quoteRepository->getActive($cartId);
        if (!$this->system->isEnabled() || !$quote->getIsMultiShipping()) {
            return $proceed($cartId);
        }
        if ($quote->isVirtual()) {
            $addressTotalsData = $quote->getBillingAddress()->getData();
            $addressTotals = $quote->getBillingAddress()->getTotals();
        } else {
            $addressTotalsData = $this->getAddressTotalData($quote);
            $addressTotals = $this->getAddressTotals($quote);
        }
        unset($addressTotalsData[ExtensibleDataInterface::EXTENSION_ATTRIBUTES_KEY]);

        /** @var \Magento\Quote\Api\Data\TotalsInterface $quoteTotals */
        $quoteTotals = $this->totalsFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $quoteTotals,
            $addressTotalsData,
            \Magento\Quote\Api\Data\TotalsInterface::class
        );
        $items = [];
        foreach ($quote->getAllVisibleItems() as $index => $item) {
            $items[$index] = $this->itemConverter->modelToDataObject($item);
        }
        $calculatedTotals = $this->totalsConverter->process($addressTotals);
        $quoteTotals->setTotalSegments($calculatedTotals);

        $amount = $quoteTotals->getGrandTotal() - $quoteTotals->getTaxAmount();
        $amount = $amount > 0 ? $amount : 0;
        $quoteTotals->setCouponCode($this->couponService->get($cartId));
        $quoteTotals->setGrandTotal($amount);
        $quoteTotals->setItems($items);
        $quoteTotals->setItemsQty($quote->getItemsQty());
        $quoteTotals->setBaseCurrencyCode($quote->getBaseCurrencyCode());
        $quoteTotals->setQuoteCurrencyCode($quote->getQuoteCurrencyCode());
        return $quoteTotals;
    }

    /**
     * @param Quote $quote
     * @return array
     */
    public function getAddressTotalData(Quote $quote)
    {
        $addressTotals = [];
        foreach ($quote->getAllShippingAddresses() as $shippingAddress) {
            foreach($shippingAddress->getData() as $key => $data) {
                if (in_array($key,$this->total)) {
                    if (array_key_exists($key,$addressTotals)) {
                        $addressTotals[$key] += $shippingAddress[$key];
                        continue;
                    }
                }
                $addressTotals[$key] = $data;
            }
        }
        return $addressTotals;
    }

    /**
     * @param Quote $quote
     * @return array
     */
    public function getAddressTotals(Quote $quote)
    {
        $totals = [];
        foreach ($quote->getAllShippingAddresses() as $shippingAddress) {
            foreach($shippingAddress->getTotals() as $total) {
                if (array_key_exists($total->getCode(),$totals)) {
                    foreach ($total->getData() as $key => $data) {
                        if (!in_array($key,$this->exclude_totals)) {
                            $totalsData = $totals[$total->getCode()];
                            $totalsData->setData($key,$totalsData->getData($key) + $data);
                            $totals[$total->getCode()] = $totalsData;
                        }
                    }
                    continue;
                }
                $totals[$total->getCode()] = $total;
            }
        }
        return $totals;
    }
}
