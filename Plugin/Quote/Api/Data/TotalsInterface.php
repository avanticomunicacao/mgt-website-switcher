<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Quote\Api\Data;

use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Checkout\Model\Session;

/**
 * Class ItemStoreGet
 *
 * @package Avanti\WebsiteSwitcher\Plugin\ItemStoreGet
 */
class TotalsInterface
{
    /**
     * @var System
     */
    private $system;

    /**
     * @var Session
     */
    private $_checkoutSession;

    /**
     * @var Data
     */
    private $data;

    /**
     * ItemStoreGet constructor.
     * @param System $system
     * @param Session $_checkoutSession
     * @param Data $data
     */
    public function __construct(
        System $system,
        Session $_checkoutSession,
        Data $data
    ) {
        $this->system = $system;
        $this->_checkoutSession = $_checkoutSession;
        $this->data = $data;
    }

    /**
     * @param \Magento\Quote\Api\Data\TotalsInterface $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetItems(
        \Magento\Quote\Api\Data\TotalsInterface $subject,
        $result
    ) {
        if (!$this->system->isEnabled()) {
            return $result;
        }
        try {
            foreach ($result as $item) {
                $extensionAttributes = $item->getExtensionAttributes();
                $extensionAttributes->setStoreId($this->_checkoutSession->getQuote()->getItemById($item->getItemId())->getStoreId());
                $extensionAttributes->setStoreName($this->data->getStoreById($this->_checkoutSession->getQuote()->getItemById($item->getItemId())->getStoreId())->getName());
                $item->setExtensionAttributes($extensionAttributes);
            }
        } catch (NoSuchEntityException $e) {
            return $result;
        }
        return $result;
    }
}