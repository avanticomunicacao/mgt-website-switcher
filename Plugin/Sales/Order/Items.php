<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Sales\Order;

use Avanti\WebsiteSwitcher\Block\Checkout\Success;

/**
 * Class Items
 * Interceptor to \Magento\Sales\Block\Order\Items
 */
class Items
{

    /**
     * @var Success
     */
    private $blockSuccess;

    /**
     * Items constructor.
     * @param Success $blockSuccess
     */
    public function __construct(
        Success $blockSuccess
    ) {
        $this->blockSuccess = $blockSuccess;
    }

    /**
     * @param \Magento\Sales\Block\Order\Items $subject
     * @param $result
     * @return float|\Magento\Sales\Api\Data\OrderItemInterface[]|null
     */
    public function afterGetItems(
        \Magento\Sales\Block\Order\Items $subject,
        $result
    ) {
        if ($this->blockSuccess->getOrderArray()) {
            $order = $subject->getOrder();
            return $order->getItems();
        }
        return $result;
    }
}