<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Swatches\Block\Product\Renderer;

use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Avanti\WebsiteSwitcher\Helper\Config;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Block\Product\Context;
use Magento\Catalog\Helper\Product as CatalogProduct;
use Magento\Catalog\Model\Product\Image\UrlBuilder;
use Magento\ConfigurableProduct\Model\ConfigurableAttributeData;
use Magento\Customer\Helper\Session\CurrentCustomer;
use Magento\Framework\Json\EncoderInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\Stdlib\ArrayUtils;
use Magento\Swatches\Helper\Data as SwatchData;
use Magento\Swatches\Helper\Media;
use Magento\Swatches\Model\SwatchAttributesProvider;

/**
 * Class Configurable
 */
class Configurable extends \Magento\Swatches\Block\Product\Renderer\Configurable
{
    /**
     * @var System
     */
    private $system;

    /**
     * @var Data
     */
    private $data;
    /**
     * @var \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable
     */
    private $configurable;
    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Configurable constructor.
     * @param Context $context
     * @param ArrayUtils $arrayUtils
     * @param EncoderInterface $jsonEncoder
     * @param \Magento\ConfigurableProduct\Helper\Data $helper
     * @param CatalogProduct $catalogProduct
     * @param CurrentCustomer $currentCustomer
     * @param PriceCurrencyInterface $priceCurrency
     * @param ConfigurableAttributeData $configurableAttributeData
     * @param SwatchData $swatchHelper
     * @param Media $swatchMediaHelper
     * @param array $data
     * @param SwatchAttributesProvider|null $swatchAttributesProvider
     * @param UrlBuilder|null $imageUrlBuilder
     * @param System $system
     * @param Data $helperData
     * @param \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $configurable
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        Context $context,
        ArrayUtils $arrayUtils,
        EncoderInterface $jsonEncoder,
        \Magento\ConfigurableProduct\Helper\Data $helper,
        CatalogProduct $catalogProduct,
        CurrentCustomer $currentCustomer,
        PriceCurrencyInterface $priceCurrency,
        ConfigurableAttributeData $configurableAttributeData,
        SwatchData $swatchHelper,
        Media $swatchMediaHelper,
        SwatchAttributesProvider $swatchAttributesProvider = null,
        UrlBuilder $imageUrlBuilder = null,
        System $system,
        Data $helperData,
        \Magento\ConfigurableProduct\Block\Product\View\Type\Configurable $configurable,
        ProductRepositoryInterface $productRepository,
        array $data = []
    ) {
        parent::__construct(
            $context,
            $arrayUtils,
            $jsonEncoder,
            $helper,
            $catalogProduct,
            $currentCustomer,
            $priceCurrency,
            $configurableAttributeData,
            $swatchHelper,
            $swatchMediaHelper,
            $data,
            $swatchAttributesProvider,
            $imageUrlBuilder
        );
        $this->system = $system;
        $this->data = $helperData;
        $this->configurable = $configurable;
        $this->productRepository = $productRepository;
    }

    public function beforeGetProduct(
        \Magento\Swatches\Block\Product\Renderer\Configurable $subject
    ) {
        if ($this->system->isEnabled()) {
            $product = $this->checkProduct();
            if (!is_null($product) && (int)$product->getStoreId() != (int)$this->data->getStoreId()) {
                $product = $this->productRepository->get(
                    $this->$product->getSku(),
                    false,
                    (int)$this->data->getStoreId(),
                    true);
                $subject->setProduct($product);
            }
        }
        return [];
    }

    /**
     * @return \Magento\Catalog\Model\Product
     */
    public function checkProduct()
    {
        if (!$this->product) {
            return $this->configurable->getProduct();
        }
        return $this->product;
    }

    /**
     * @param \Magento\Swatches\Block\Product\Renderer\Configurable $subject
     * @param $result
     * @return string
     */
    public function afterGetCacheKey(
        \Magento\Swatches\Block\Product\Renderer\Configurable $subject,
        $result
    ) {
        if ($this->system->isEnabled()) {
            $cacheKey = $result . '-' . $this->data->getStoreId();
            return $result . '-' . $this->data->getStoreId();
        }
        return $result;
    }

    /**
     * @param \Magento\Swatches\Block\Product\Renderer\Configurable $subject
     * @return array
     */
    public function beforeGetAllowProducts(
        \Magento\Swatches\Block\Product\Renderer\Configurable $subject
    ) {
        if ($this->system->isEnabled()) {
            $subject->unsetData('allow_products');
        }
        return [];
    }
}