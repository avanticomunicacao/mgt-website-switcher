<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\ConfigurableProduct\Model\Product\Type;

use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\Product;
use Magento\Customer\Model\Session;
use Magento\Framework\Config\CacheInterface;
use Magento\Framework\EntityManager\MetadataPool;
use Magento\Framework\App\ObjectManager;

/**
 * Class Configurable
 */
class Configurable
{
    /**
     * @var System
     */
    private $system;

    /**
     * @var ProductInterface
     */
    private $_product;

    /**
     * @var Session
     */
    private $_customerSession;

    /**
     * @var MetadataPool
     */
    private $_metadataPool;

    /**
     * @var CacheInterface
     */
    private $_frontendCache;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var ProductRepositoryInterface
     */
    private $productRepository;

    /**
     * Configurable constructor.
     * @param System $system
     * @param ProductInterface $product
     * @param Session $_customerSession
     * @param CacheInterface $_frontendCache
     * @param Data $data
     * @param ProductRepositoryInterface $productRepository
     */
    public function __construct(
        System $system,
        ProductInterface $product,
        Session $_customerSession,
        CacheInterface $_frontendCache,
        Data $data,
        ProductRepositoryInterface $productRepository
    ) {
        $this->system = $system;
        $this->_product = $product;
        $this->_customerSession = $_customerSession;
        $this->_frontendCache = $_frontendCache;
        $this->data = $data;
        $this->productRepository = $productRepository;
    }

    /**
     * Returns array of sub-products for specified configurable product
     *
     * $requiredAttributeIds - one dimensional array, if provided
     *
     * Result array contains all children for specified configurable product
     *
     * @param  Product $product
     * @param  array $requiredAttributeIds
     * @return ProductInterface[]
     */
    public function aroundGetUsedProducts(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject,
        \Closure $proceed,
        Product $product,
        $requiredAttributeIds = null
    ) {
        $result = $proceed($product,$requiredAttributeIds);
        if ($this->system->isEnabled()) {
            $this->removeCache([
                'product' => $product,
                'requiredAttributeIds' => $requiredAttributeIds
            ]);
        }
        return $result;
    }

    /**
     * Returns array of sub-products for specified configurable product filtered by salable status
     *
     * Result array contains only those children for specified configurable product which are salable on store front
     *
     * @deprecated 100.2.0 Not used anymore. Keep it for backward compatibility.
     *
     * @param Product $product
     * @param array|null $requiredAttributeIds
     * @return ProductInterface[]
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     * @since 100.1.3
     */
    public function aroundGetSalableUsedProducts(
        \Magento\ConfigurableProduct\Model\Product\Type\Configurable $subject,
        \Closure $proceed,
        Product $product,
        $requiredAttributeIds = null
    ) {
        $result = $proceed($product,$requiredAttributeIds);
        if ($this->system->isEnabled()) {
            $this->removeCache([
                'product' => $product,
                'requiredAttributeIds' => $requiredAttributeIds
            ]);
        }
        return $result;
    }

    /**
     * @param $value
     */
    public function removeCache($value)
    {
        $cacheKey = $this->getCacheKey($value['product'],$value['requiredAttributeIds']);
        $this->_frontendCache->remove($cacheKey);
    }


    /**
     * @param $product
     * @param null $requiredAttributeIds
     * @return string
     * @throws \Exception
     */
    public function getCacheKey($product, $requiredAttributeIds = null)
    {
        $method = Config::METHOD_CONFIGURABLE_PRODUCT_MODEL_GET_USED_PRODUCTS;
        $metadata = $this->getMetadataPool()->getMetadata(ProductInterface::class);
        if ($requiredAttributeIds !== null) {
            $method = Config::METHOD_CONFIGURABLE_PRODUCT_MODEL_GET_SALABLE_USED_PRODUCTS;
        }
        $keyParts = [
            $method,
            $product->getData($metadata->getLinkField()),
            $product->getStoreId(),
            $this->_customerSession->getCustomerGroupId(),
        ];
        if ($requiredAttributeIds !== null) {
            sort($requiredAttributeIds);
            $keyParts[] = implode('', $requiredAttributeIds);
        }
        return $this->getUsedProductsCacheKey($keyParts);
    }

    /**
     * Create string key based on $keyParts
     *
     * $keyParts - one dimensional array of strings
     *
     * @param array $keyParts
     * @return string
     */
    private function getUsedProductsCacheKey($keyParts)
    {
        return sha1(implode('_', $keyParts));
    }

    /**
     * Get MetadataPool instance
     * @return MetadataPool
     */
    private function getMetadataPool()
    {
        if (!$this->_metadataPool) {
            $this->_metadataPool = ObjectManager::getInstance()->get(MetadataPool::class);
        }
        return $this->_metadataPool;
    }
}