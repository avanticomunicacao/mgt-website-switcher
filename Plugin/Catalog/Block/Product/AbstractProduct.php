<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Catalog\Block\Product;

use Magento\Catalog\Block\Product\Context;

/**
 * Class AbstractProduct
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Magento\Catalog\Block\Product
 */
class AbstractProduct
{

    /**
     * @var \Magento\Framework\Registry
     */
    private $_coreRegistry;

    public function __construct(
        Context $context
    ) {
        $this->_coreRegistry = $context->getRegistry();
    }

    /**
     * @param \Magento\Catalog\Block\Product\AbstractProduct $subject
     * @param $result
     * @return mixed
     */
    public function afterGetProduct(
        \Magento\Catalog\Block\Product\AbstractProduct $subject,
        $result
    ) {
        $product = $this->_coreRegistry->registry('product');
        if (!is_null($product)) {
            return $product;
        }
        return $result;
    }
}