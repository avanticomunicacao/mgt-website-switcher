<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Checkout\Controller\Cart;

use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Checkout\Helper\Cart;

class Add
{
    /**
     * @var Avanti\WebsiteSwitcher\Helper\Data
     */
    protected $data;

    /**
     * @var
     */
    protected $originStoreId;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var System
     */
    private $system;

    /**
     * Add constructor.
     * @param Cart $cart
     * @param Data $data
     * @param System $system
     */
    public function __construct(
        Cart $cart,
        Data $data,
        System $system
    ) {
        $this->data = $data;
        $this->cart = $cart;
        $this->originStoreId = $this->data->getStoreId();
        $this->system = $system;
    }

    /**
     * Add product to shopping cart action
     */
    public function aroundExecute(
        \Magento\Checkout\Controller\Cart\Add $subject,
        \Closure $proceed
    ) {
        $params = $subject->getRequest()->getParams();
        if (isset($params['store_id'])) {
            $this->data->changeStore($params['store_id']);
            if (!$this->system->updateCartInAdd()) {
                $this->updateCartItem($params);
            }
        }
        $result = $proceed();
        if (isset($params['store_id'])) {
            $this->data->changeStore($this->originStoreId);
        }
        return $result;
    }

    /**
     * @param $params
     */
    public function updateCartItem($params)
    {
        if ($this->cart->getQuote() && $this->cart->getQuote()->hasItems()) {
            foreach ($this->cart->getQuote()->getItems() as $item) {
                if ((int)$item->getProduct()->getId() === (int)$params['product']) {
                    $item->setStoreId($params['store_id'])
                        ->getProduct()
                        ->setStoreId($params['store_id']);
                }
                $items[] = $item;
            }
            if (isset($items)) {
                $this->cart->getQuote()->setItems($items);
            }
        }
    }
}