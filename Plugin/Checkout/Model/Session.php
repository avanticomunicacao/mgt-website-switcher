<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Checkout\Model;

use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Catalog\Model\ProductFactory;
use Magento\Framework\Session\StorageInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\ConfigurableProduct\Model\Product\Type\Configurable;

/**
 * Class Session
 *
 * @package Avanti\WebsiteSwitcher\Plugin\Magento\Checkout\Model\Session
 */
class Session
{
    /**
     * Storage
     *
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var ProductFactory
     */
    private $_productFactory;
    /**
     * @var System
     */
    private $system;
    /**
     * @var ProductRepositoryInterface
     */
    private $_productRepository;
    /**
     * @var Configurable
     */
    private $configurable;

    /**
     * Session constructor.
     * @param StorageInterface $storage
     * @param StoreManagerInterface $_storeManager
     * @param ProductFactory $_productFactory
     * @param System $system
     * @param ProductRepositoryInterface $_productRepository
     */
    public function __construct(
        StorageInterface $storage,
        StoreManagerInterface $_storeManager,
        ProductFactory $_productFactory,
        System $system,
        ProductRepositoryInterface $_productRepository,
        Configurable $configurable
    ) {
        $this->storage = $storage;
        $this->_storeManager = $_storeManager;
        $this->_productFactory = $_productFactory;
        $this->system = $system;
        $this->_productRepository = $_productRepository;
        $this->configurable = $configurable;
    }

    /**
     * @param \Magento\Checkout\Model\Session $subject
     * @param $result
     * @param $quoteId
     * @return mixed
     */
    public function afterSetQuoteId(
        \Magento\Checkout\Model\Session $subject,
        $result,
        $quoteId
    ) {
        if (!is_null($this->storage->getData(Config::ALLL_QUOTE_ID))) {
            $quote = $this->storage->getData(Config::ALLL_QUOTE_ID);
        }
        $quote[$this->_storeManager->getStore()->getWebsiteId()] = $subject->getQuoteId();
        $this->storage->setData(Config::ALLL_QUOTE_ID,$quote);
        return $result;
    }

    /**
     * @param \Magento\Checkout\Model\Session $subject
     * @param $result
     * @return mixed
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterGetQuote(
        \Magento\Checkout\Model\Session $subject,
        $result
    ) {
        if ($this->system->isEnabled() && $this->system->shareQuote() && $this->checkItems($result)) {
            foreach ($result->getItems() as $item) {
                $item->setProduct($this->getProduct($item));
                $items[] = $item;
            }
        }
        if (isset($items)) {
            $result->setItems($items);
        }
        return $result;
    }

    /**
     * @param $quote
     * @return bool
     */
    public function checkItems($quote)
    {
        return ($quote->hasItems() && !empty($quote->getItems()));
    }

    /**
     * @param $sku
     * @param $storeId
     * @return \Magento\Catalog\Api\Data\ProductInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function getProduct($item)
    {
        switch ($item->getProductType()) {
            case Configurable::TYPE_CODE :
                $product =  $this->_productRepository->get(
                    $this->_productRepository->getById($item->getProductId())->getSku(),
                    false,
                    $item->getStoreId(),
                    true);
                break;
            default :
                $product =  $this->_productRepository->get(
                    $item->getSku(),
                    false,
                    $item->getStoreId(),
                    true);
        }
        return $product;
    }
}