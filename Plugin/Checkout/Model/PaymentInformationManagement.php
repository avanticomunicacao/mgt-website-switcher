<?php

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Checkout\Model;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Checkout\Model\Session as CheckoutSession;

class PaymentInformationManagement
{

    const AVANTI_WS_PAYMENT_METHOD = 'avanti_ws_payment_method';

    /**
     * @var CheckoutSession
     */
    private $_checkoutSession;

    /**
     * @var System
     */
    private $system;

    /**
     * PaymentInformationManagement constructor.
     * @param CheckoutSession $_checkoutSession
     * @param System $system
     */
    public function __construct(
        CheckoutSession $_checkoutSession,
        System $system
    ) {
        $this->_checkoutSession = $_checkoutSession;
        $this->system = $system;
    }

    /**
     * @param \Magento\Checkout\Model\PaymentInformationManagement $subject
     * @param $cartId
     * @param $paymentMethod
     * @param null $billingAddress
     * @return array
     */
    public function beforeSavePaymentInformationAndPlaceOrder(
        \Magento\Checkout\Model\PaymentInformationManagement $subject,
        $cartId,
        $paymentMethod,
        $billingAddress = null
    ) {
        if ($this->system->isEnabled()) {
            $this->_checkoutSession->getQuote()->save();
            $this->_checkoutSession->getQuote()->setData(self::AVANTI_WS_PAYMENT_METHOD,$paymentMethod);
        }
        return [$cartId, $paymentMethod, $billingAddress];
    }
}
