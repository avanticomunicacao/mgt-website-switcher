<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Checkout\Model;

use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\Data;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Checkout\Model\Session as CheckoutSession;

class DefaultConfigProvider
{

    /**
     * @var System
     */
    private $system;

    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;

    /**
     * @var CheckoutSession
     */
    private $checkoutSession;

    /**
     * @var Data
     */
    private $data;

    public function __construct(
        System $system,
        QuoteHandlerInterface $quoteHandler,
        CheckoutSession $checkoutSession,
        Data $data
    ) {
        $this->system = $system;
        $this->quoteHandler = $quoteHandler;
        $this->checkoutSession = $checkoutSession;
        $this->data = $data;
    }

    public function afterGetConfig(
        \Magento\Checkout\Model\DefaultConfigProvider $subject,
        $result
    ) {
        if (!$this->system->isEnabled()) {
            return $result;
        }
        if ($this->checkoutSession->getQuote()->getId()) {
            $quote = $this->checkoutSession->getQuote();
            if ($this->quoteHandler->splitQuotes($quote)) {
                $this->quoteHandler->splitQuoteAdressesses();
                $quote = $this->checkoutSession->getQuote();
                $quoteData = $result['quoteData'];
                $quoteData['is_multi_shipping'] = $quote->getIsMultiShipping();
                $quoteData['all_shipping_addressess'] = $this->getAllShippingAddresses($quote);
                $result['quoteData'] = $quoteData;
            }
        }
        return $result;
    }

    public function getAllShippingAddresses($quote)
    {
        $shippingAddressess = [];
        foreach ($quote->getAllShippingAddresses() as $address) {
            $shippingAddress = [
                'id' => $address->getId(),
                'store_id' => $address->getData('store_id'),
                'store_name' => $this->data->getStoreById($address->getData('store_id'))->getName(),
                'shipping_method' => $address->getShippingMethod()
            ];
            $shippingAddressess[] = $shippingAddress;
        }
        return $shippingAddressess;
    }
}
