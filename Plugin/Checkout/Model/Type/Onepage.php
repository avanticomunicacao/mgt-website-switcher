<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Checkout\Model\Type;

use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\System;

class Onepage
{
    /**
     * @var System
     */
    private $system;

    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;

    /**
     * Onepage constructor.
     * @param System $system
     * @param QuoteHandlerInterface $quoteHandler
     */
    public function __construct(
        System $system,
        QuoteHandlerInterface $quoteHandler
    ) {
        $this->system = $system;
        $this->quoteHandler = $quoteHandler;
    }

    /**
     * @param \Magento\Checkout\Model\Type\Onepage $subject
     * @return array
     */
    public function beforeInitCheckout(
        \Magento\Checkout\Model\Type\Onepage $subject
    ) {
        if ($this->system->isEnabled()) {
            $this->quoteHandler->splitQuoteAdressesses();
        }
        return [];
    }
}