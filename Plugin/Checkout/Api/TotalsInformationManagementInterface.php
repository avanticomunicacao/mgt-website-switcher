<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Plugin\Checkout\Api;

use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Checkout\Api\Data\TotalsInformationInterface;
use Magento\Checkout\Model\TotalsInformationManagement;
use Magento\Framework\App\ObjectManager;
use Magento\Quote\Api\CartRepositoryInterface;
use Magento\Quote\Api\CartTotalRepositoryInterface;
use Magento\Quote\Api\Data\CartExtensionFactory;
use Magento\Quote\Model\Quote\Address;
use Magento\Quote\Model\Quote\ShippingAssignment\ShippingAssignmentProcessor;

class TotalsInformationManagementInterface extends TotalsInformationManagement
{
    /**
     * @var ShippingAssignmentProcessor
     */
    private $shippingAssignmentProcessor;

    /**
     * @var CartExtensionFactory
     */
    private $cartExtensionFactory;

    /**
     * @var System
     */
    private $system;

    /**
     * TotalsInformationManagementInterface constructor.
     * @param System $system
     * @param CartRepositoryInterface $cartRepository
     * @param CartTotalRepositoryInterface $cartTotalRepository
     * @param CartExtensionFactory|null $cartExtensionFactory
     */
    public function __construct(
        System $system,
        CartRepositoryInterface $cartRepository,
        CartTotalRepositoryInterface $cartTotalRepository,
        CartExtensionFactory $cartExtensionFactory = null
    ) {
        parent::__construct(
            $cartRepository,
            $cartTotalRepository
        );
        $this->system = $system;
        $this->cartExtensionFactory = $cartExtensionFactory ?: ObjectManager::getInstance()
            ->get(CartExtensionFactory::class);
    }

    /**
     * @param \Magento\Checkout\Api\TotalsInformationManagementInterface $subject
     * @param \Closure $proceed
     * @param $cartId
     * @param TotalsInformationInterface $addressInformation
     * @return \Magento\Quote\Api\Data\TotalsInterface|mixed
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function aroundCalculate(
        \Magento\Checkout\Api\TotalsInformationManagementInterface $subject,
        \Closure $proceed,
        $cartId,
        TotalsInformationInterface $addressInformation
    ) {
        /** @var \Magento\Quote\Model\Quote $quote */
        $quote = $this->cartRepository->get($cartId);
        if (!$this->system->isEnabled()) {
            return $proceed($cartId, $addressInformation);
        }
        if (!$quote->getIsMultiShipping()) {
            $this->validateQuote($quote);
            if ($quote->getIsVirtual()) {
                $quote->setBillingAddress($addressInformation->getAddress());
            } else {
                $quote->setShippingAddress($addressInformation->getAddress());
                $quote->getShippingAddress()->setCollectShippingRates(true)->setShippingMethod(
                    $addressInformation->getShippingCarrierCode() . '_' . $addressInformation->getShippingMethodCode()
                );
            }
            $quote->collectTotals();
            $this->cartRepository->save($quote);
            return $this->cartTotalRepository->get($cartId);
        }
        $this->validateQuote($quote);
        if ($quote->getIsVirtual()) {
            $quote->setBillingAddress($addressInformation->getAddress());
        } else {
            foreach ($quote->getAllShippingAddresses() as $shippingAddress) {
                if ($shippingAddress) {
                    $storeIdItem = $this->getStoreIdByItem($shippingAddress);
                    if ($storeIdItem === $addressInformation->getExtensionAttributes()->getStoreId()) {
                        $shippingAddress->setCollectShippingRates(true)->setShippingMethod(
                            $addressInformation->getShippingCarrierCode() . '_' . $addressInformation->getShippingMethodCode()
                        );
                    }
                }
            }
        }
        $this->prepareShippingAssignment($quote);
        $quote->collectTotals();
        $this->cartRepository->save($quote);
        return $this->cartTotalRepository->get($cartId);
    }

    /**
     * @param Address $address
     * @return int
     */
    public function getStoreIdByItem(Address $address)
    {
        foreach ($address->getAllVisibleItems() as $item) {
            if ($item->getStoreId()) {
                return (int)$item->getStoreId();
            }
        }
    }

    /**
     * Prepare shipping assignment.
     *
     * @param \Magento\Quote\Model\Quote $quote
     * @return \Magento\Quote\Model\Quote
     */
    private function prepareShippingAssignment($quote)
    {
        $cartExtension = $quote->getExtensionAttributes();
        if ($cartExtension === null) {
            $cartExtension = $this->cartExtensionFactory->create();
        }
        /** @var \Magento\Quote\Api\Data\ShippingAssignmentInterface $shippingAssignment */
        $shippingAssignment = $this->getShippingAssignmentProcessor()->create($quote);
        $shipping = $shippingAssignment->getShipping();

        $shipping->setMethod(null);
        $shippingAssignment->setShipping($shipping);
        $cartExtension->setShippingAssignments([$shippingAssignment]);
        return $quote->setExtensionAttributes($cartExtension);
    }

    /**
     * Get shipping assignment processor.
     *
     * @return ShippingAssignmentProcessor
     */
    private function getShippingAssignmentProcessor()
    {
        if (!$this->shippingAssignmentProcessor) {
            $this->shippingAssignmentProcessor = ObjectManager::getInstance()
                ->get(ShippingAssignmentProcessor::class);
        }
        return $this->shippingAssignmentProcessor;
    }
}
