<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Controller\Website;

use Avanti\WebsiteSwitcher\Api\CacheInterface;
use Avanti\WebsiteSwitcher\Api\CartInterface;
use Avanti\WebsiteSwitcher\Api\QuoteRepositoryInterface;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\App\Action\Action;
use Avanti\WebsiteSwitcher\Api\CookieInterface;
use Avanti\WebsiteSwitcher\Helper\Data;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class ChangeAction extends Action
{

    /**
     * @var CookieInterface
     */
    private $cookie;

    /**
     * @var Data
     */
    private $data;

    /**
     * @var CartInterface
     */
    private $cart;
    /**
     * @var CacheInterface
     */
    private $cache;

    /**
     * Initialize dependencies
     * @param CookieInterface $cookie
     * @param Context $context
     * @param Data $data
     * @param CartInterface $cart
     * @param CacheInterface $cache
     */
    public function __construct(
        CookieInterface $cookie,
        Context $context,
        Data $data,
        CartInterface $cart,
        CacheInterface $cache
    ) {
        parent::__construct($context);
        $this->cookie = $cookie;
        $this->data = $data;
        $this->cart = $cart;
        $this->cache = $cache;
    }

    /**
     * Change website action
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        if ($this->getRequest()->getParam('website')) {
            $website = (string)$this->getRequest()->getParam('website');
            try {
                $this->cache->clearCache(['full_page']);
                $this->cookie->set($website);
                $this->cart->updateCart($website);
            } catch (LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong with change website.'));
            }
        }
        /** @var \Magento\Framework\Controller\Result\Redirect $redirect */
        $redirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
        $redirectUrl = $this->_redirect->getRedirectUrl();
        return $redirect->setUrl($redirectUrl);
    }

}