<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Controller\Checkout;

use Avanti\WebsiteSwitcher\Api\QuoteHandlerInterface;
use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\System;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping\State;
use Magento\Quote\Model\QuoteFactory;
use Magento\Sales\Api\OrderRepositoryInterface;

/**
 * Multishipping checkout success controller.
 */
class Success extends Action
{
    /**
     * @var State
     */
    private $state;

    /**
     * @var Multishipping
     */
    private $multishipping;

    /**
     * @var System
     */
    private $system;
    /**
     * @var Session
     */
    private $_checkoutSession;
    /**
     * @var QuoteHandlerInterface
     */
    private $quoteHandler;
    /**
     * @var OrderRepositoryInterface
     */
    private $orderRepository;
    /**
     * @var QuoteFactory
     */
    private $_quoteFactory;

    /**
     * @param Context $context
     * @param State $state
     * @param Multishipping $multishipping
     * @param System $system
     * @param Session $checkoutSession
     * @param QuoteHandlerInterface $quoteHandler
     * @param OrderRepositoryInterface $orderRepository
     * @param QuoteFactory $_quoteFactory
     */
    public function __construct(
        Context $context,
        State $state,
        Multishipping $multishipping,
        System $system,
        Session $checkoutSession,
        QuoteHandlerInterface $quoteHandler,
        OrderRepositoryInterface $orderRepository,
        QuoteFactory $_quoteFactory
    ) {
        $this->state = $state;
        $this->multishipping = $multishipping;
        parent::__construct($context);
        $this->system = $system;
        $this->_checkoutSession = $checkoutSession;
        $this->quoteHandler = $quoteHandler;
        $this->orderRepository = $orderRepository;
        $this->_quoteFactory = $_quoteFactory;
    }

    /**
     * Multishipping checkout success page
     *
     * @return void
     */
    public function execute()
    {
        if (!$this->state->getCompleteStep(Config::STEP_OVERVIEW)) {
            $this->_redirect('*/*/shipping');
            return;
        }
        $ids = $this->multishipping->getOrderIds();
        $this->_eventManager->dispatch('multishipping_avanti_checkout_controller_success_action', ['order_ids' => $ids]);
        switch ($this->system->successPage()) {
            case Config::ONE_PAGE:
                foreach ($ids as $id) {
                    $order = $this->orderRepository->get($id);
                    $orderIds[$id] = $order->getIncrementId();
                }
                $quote = $this->_quoteFactory->create()->load($order->getQuoteId());
                $quote->setId($order->getQuoteId());
                $this->quoteHandler->defineSessions($quote,$order,$orderIds);
                $this->_redirect('checkout/onepage/success/');
                return;
        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
