<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Controller\Checkout;

use Avanti\WebsiteSwitcher\Api\Checkout\Type\MultishippingInterface;
use Avanti\WebsiteSwitcher\Controller\Checkout;
use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\Url;
use Avanti\WebsiteSwitcher\Model\Checkout\CheckItems;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping\State;
use Magento\Checkout\Model\Session as CheckoutSession;
use Magento\Customer\Api\AccountManagementInterface;
use Magento\Customer\Api\CustomerRepositoryInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\ResponseInterface;

class Shipping extends Checkout
{

    /**
     * @var CheckItems
     */
    private $checkItems;

    /**
     * Shipping constructor.
     * @param Context $context
     * @param Session $customerSession
     * @param CustomerRepositoryInterface $customerRepository
     * @param AccountManagementInterface $accountManagement
     * @param MultishippingInterface $multishipping
     * @param Url $url
     * @param CheckoutSession $_session
     * @param State $state
     * @param CheckItems $checkItems
     */
    public function __construct(
        Context $context,
        Session $customerSession,
        CustomerRepositoryInterface $customerRepository,
        AccountManagementInterface $accountManagement,
        MultishippingInterface $multishipping,
        Url $url,
        CheckoutSession $_session,
        State $state,
        CheckItems $checkItems
    ) {
        parent::__construct(
            $context,
            $customerSession,
            $customerRepository,
            $accountManagement,
            $multishipping,
            $url,
            $_session,
            $state
        );
        $this->checkItems = $checkItems;
    }

    /**
     * Multishipping checkout shipping information page
     *
     * @return  ResponseInterface|void
     */
    public function execute()
    {
        // If customer do not have addresses
        if (!$this->_getCheckout()->getCustomerDefaultShippingAddress()) {
            $this->_redirect('customer/address/');
            $this->messageManager->addNotice(__('You must have at least one delivery address'));
            return;
        }
        if (!$this->_validateMinimumAmount()) {
            return;
        }
        $this->checkItems->execute();
        $this->_getState()->setCompleteStep(Config::STEP_SELECT_ADDRESSES);
        $this->_getState()->setActiveStep(Config::STEP_SHIPPING);
        if (!$this->_getCheckout()->validateMinimumAmount()) {
            $message = $this->_getCheckout()->getMinimumAmountDescription();
            $this->messageManager->addNotice($message);
        }
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
