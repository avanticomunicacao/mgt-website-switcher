<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Controller\Checkout;

use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping;
use Avanti\WebsiteSwitcher\Model\Checkout\Type\Multishipping\State;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\RequestInterface;

/**
 * Multishipping checkout controller Address
 */
abstract class Address extends Action
{
    /**
     * @var Multishipping
     */
    private $multishipping;
    /**
     * @var State
     */
    private $state;

    /**
     * Address constructor.
     * @param Multishipping $multishipping
     * @param State $state
     */
    public function __construct(
        Multishipping $multishipping,
        State $state
    ) {

        $this->multishipping = $multishipping;
        $this->state = $state;
    }

    /**
     * {@inheritdoc}
     */
    public function dispatch(RequestInterface $request)
    {
        if (!$this->_getCheckout()->getCustomer()->getId()) {
            return $this->_redirect('customer/account/login');
        }
        return parent::dispatch($request);
    }

    /**
     * Retrieve multishipping checkout model
     *
     * @return Multishipping
     */
    protected function _getCheckout()
    {
        return $this->multishipping;
    }

    /**
     * Retrieve checkout state model
     *
     * @return State
     */
    protected function _getState()
    {
        return $this->state;
    }
}