<?php
/**
 * Avanti
 *
 * NOTICE OF LICENSE
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to https://www.penseavanti.com.br for more information.
 *
 * @module      Avanti Website Switcher
 * @category    Avanti
 * @package     Avanti_WebsiteSwitcher
 *
 * @copyright   Copyright (c) 2019 Avanti. (https://www.penseavanti.com.br)
 *
 * @author      Avanti Core Team <contato@penseavanti.com.br>
 */

declare(strict_types=1);

namespace Avanti\WebsiteSwitcher\Console\Command;

use Avanti\WebsiteSwitcher\Helper\Config;
use Avanti\WebsiteSwitcher\Helper\System;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class Generate index
 */
class GenerateIndex extends Command
{
    /**
     * @var System
     */
    private $system;

    /**
     * GenerateIndex constructor.
     * @param string|null $name
     * @param System $system
     */
    public function __construct(
        string $name = null,
        System $system
    ) {
        parent::__construct($name);
        $this->system = $system;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this->setName('index:generate')
            ->setDescription('generate custom index2.php in pub folder using the file pub/index.php');

        parent::configure();
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->writeln('<info>Executing<info>');
        $index = file_get_contents(Config::FILE_PUB_INDEX);
        $explodedFile = explode('$params = $_SERVER;',$index);
        $index2 = $explodedFile[0].'
        $params = $_SERVER;
        if (isset($_COOKIE[\'' . $this->system->cookieName() . '\'])) {
            $params[\Magento\Store\Model\StoreManager::PARAM_RUN_CODE] =  $_COOKIE[\'' . $this->system->cookieName() . '\'];
            $params[\Magento\Store\Model\StoreManager::PARAM_RUN_TYPE] = \'website\';
        }
        '.$explodedFile[1];
        $file = fopen(Config::FILE_PUB_INDEX2,"w") or die($output->writeln('<error>Unable to open file INDEX2!<error>'));
        fwrite($file, $index2);
        $output->writeln('<info>index2.php successfully generated in the pub folder<info>');
    }
}